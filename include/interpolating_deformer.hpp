#ifndef INTERPOLATING_DEFORMER_HPP_INCLUDED
#define INTERPOLATING_DEFORMER_HPP_INCLUDED

#include <deformer.hpp>
#include <raytracing.hpp>
#include <string>
#include <vector>

class interpolating_deformer: public deformer{
public:
  //z coordinate will always be set to z
  interpolating_deformer(std::string filename, double maxelong, double z_zero, double up, point3 center, double radius=0.5);
  virtual double z(double x, double y) const override;

private:
  std::vector<double> rs;
  std::vector<double> zs;
  double maxelong;
  double z_zero;
  double up;
  point3 center;
  double radius;
};

#endif
