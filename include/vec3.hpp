#ifndef VEC3_HPP_INCLUDED
#define VEC3_HPP_INCLUDED

#include <cmath>
#include <iostream>

class vec3{
public:
  vec3();
  vec3(double x, double y, double z);

  double x() const;
  double y() const;
  double z() const;

  vec3 operator-() const;
  double operator[](int i) const;
  double& operator[](int i);

  vec3& operator+=(const vec3&);
  vec3& operator-=(const vec3&);
  vec3& operator*=(const double);
  vec3& operator/=(const double);

  double length() const;
  double length_squared() const;
  bool near_zero() const;

  static vec3 random();
  static vec3 random(double min, double max);

private:
  double e[3];
};

using point3 = vec3;
using color = vec3;

/*Utility*/
std::ostream& operator<<(std::ostream & out, const vec3& v);

vec3 operator+(const vec3 lhs, const vec3& rhs);

vec3 operator-(const vec3 lhs, const vec3& rhs);

vec3 operator*(const vec3 lhs, const vec3& rhs);

vec3 operator*(vec3 lhs, const double t);

vec3 operator*(const double t, vec3 rhs);

vec3 operator/(vec3 v, double t);

double dot(const vec3& lhs, const vec3& rhs);

vec3 cross(const vec3& lhs, const vec3& rhs);

vec3 unit_vector(vec3 v);

vec3 random_in_unit_sphere();

/*n points up, v points down*/
vec3 reflect(const vec3& v, const vec3& n);

/*n points up, v points down*/
/*refraction_ratio = n_old / n_new */
vec3 refract(const vec3& unit_v, const vec3& n, double refraction_ratio);

#endif
