#ifndef RAY_HPP_INCLUDED
#define RAY_HPP_INCLUDED

#include <vec3.hpp>

class ray{
public:
  ray();
  ray(const point3& origin, const vec3& direction, double amplitude, double opl, double refractive_index, int phase_flips, int max_bounces);

  point3 origin() const;
  vec3 direction() const;

  point3 at(double t) const;

  double optical_path_to(double t) const;
  double refractive_index_current() const;

  int get_bounces_left() const;
  int get_phaseflips() const;
  void set_source(int n);
  int get_source() const;

  double get_amplitude() const;

private:
  point3 orig;
  vec3 dir;
  double optical_path; //optical path since pixel
  double refractive_index; //refractive index of next straight path
  double amplitude; //amplitude left in the ray
  int phase_flips; //number of phase flips accumulated so far (from refl.)
  int source; //information about the source the ray ended at (0 for none)
  int bounces_left; //number of bounces the ray can perform
};

#endif
