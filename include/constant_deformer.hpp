#ifndef CONSTANT_DEFORMER_HPP_INCLUDED
#define CONSTANT_DEFORMER_HPP_INCLUDED

#include <deformer.hpp>

class constant_deformer: public deformer{
public:
  //z coordinate will always be set to z
  constant_deformer(double z);
  virtual double z(double x, double y) const override;

private:
  double z_const;
};

#endif
