#ifndef TRIANGLE_HPP_INCLUDED
#define TRIANGLE_HPP_INCLUDED

#include <hittable.hpp>
#include <vec3.hpp>
#include <material.hpp>
#include <memory>

/*normals always point towards the lower refractive index (i.e. outwards)*/
/*c1, c2, c3 are to be given in anti-clockwise direction as seen from outside*/
class triangle: public hittable{
public:
  triangle();
  /*normal = (c2-c1) x (c3-c1)*/
  triangle(point3 c1, point3 c2, point3 c3, std::shared_ptr<material> m);

  virtual bool hit(
    const ray& r, double t_min, double t_max, hit_record& rec) const override;

private:
  point3 c1, c2, c3; /*corners of the triangle*/
  vec3 normal; /*normal on the triangle, facing outwards*/
  double D; /*offset of plane, E: A*nx+B*ny+C*nz+D=0 */
  vec3 c1c2, c1c3; /*in-plane vectors*/
  std::shared_ptr<material> mat_ptr; /*material bounded by triangle*/
};

#endif
