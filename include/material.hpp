#ifndef MATERIAL_HPP_INCLUDED
#define MATERIAL_HPP_INCLUDED

#include <raytracing.hpp>
#include <hittable.hpp>

class material{
public:
  /*returns true if ray was scattered, false if it arrived at a source or was absorbed*/
  /* return    fraction_reflected  fraction_refracted    meaning
   * true      !=0                 !=0                   ray was reflected and refracted, results in r_reflected and r_refracted
   * true      0                   1.                    everything was refracted, result in r_refracted
   * true      1.                  0.                    everything was reflected, result in r_reflected
   * false     0.                  X                     ray was absorbed, bounce limit reached
   * false     1.                  X                     ray hit source, final ray in r_reflected
   *
  */
  virtual bool scatter(
    const ray& r_in, const hit_record& rec,
    double& fraction_reflected, ray& r_reflected,
    double& fraction_refracted, ray& r_refracted
  ) const = 0;
};

#endif
