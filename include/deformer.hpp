#ifndef DEFORMER_HPP_INCLUDED
#define DEFORMER_HPP_INCLUDED

class deformer{
public:
  virtual double z(double x, double y) const = 0;
};

#endif
