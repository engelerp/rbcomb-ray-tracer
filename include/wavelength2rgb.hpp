#ifndef WAVELENGTH2RGB_HPP_INCLUDED
#define WAVELENGTH2RGB_HPP_INCLUDED
#include <vec3.hpp>

class wavelength2rgb{
public:
  wavelength2rgb(double nm_min, double nm_max, double nm_step);

  color operator()(double nm) const;

private:
  double nm_min;
  double nm_max;
  double nm_step;
};

#endif
