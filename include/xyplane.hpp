#ifndef XYPLANE_HPP_INCLUDED
#define XYPLANE_HPP_INCLUDED

#include <hittable.hpp>
#include <memory>

class xyplane: public hittable{
public:
  xyplane();
  xyplane(double z, std::shared_ptr<material> mat_ptr);

  virtual bool hit(
    const ray& r, double t_min, double t_max, hit_record& rec) const override;

private:
  const double z;
  vec3 normal;
  std::shared_ptr<material> mat_ptr;
};

#endif
