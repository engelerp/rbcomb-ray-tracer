#ifndef DIELECTRIC_HPP_INCLUDED
#define DIELECTRIC_HPP_INCLUDED

#include <material.hpp>

class dielectric: public material{
public:
  dielectric(double index_of_refraction_inside, double index_of_refraction_outside);

  virtual bool scatter(
    const ray& r_in, const hit_record& rec,
    double& fraction_reflected, ray& r_reflected,
    double& fraction_refracted, ray& r_refracted
  ) const override;

private:
  double reflectance(double cos_theta_in, double cos_theta_out, double ir_in, double ir_out) const;
  double transmittance(double cos_theta_in, double cos_theta_out, double ir_in, double ir_out) const;

  double ir_inside; //index of refraction inside material
  double ir_outside; //index of refraction outside material
};

#endif
