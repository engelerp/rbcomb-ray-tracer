#ifndef HITTABLE_HPP_INCLUDED
#define HITTABLE_HPP_INCLUDED

#include <raytracing.hpp>

class material;

struct hit_record{
  point3 p;
  vec3 normal;
  std::shared_ptr<material> mat_ptr;
  double t;
  bool inbound; /*ray towards material? (direction ~= -normal)*/
};

class hittable{
public:
  virtual bool hit(const ray& r, double t_min, double t_max, hit_record& rec) const = 0;
};

#endif
