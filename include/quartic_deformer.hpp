#ifndef QUARTIC_DEFORMER_HPP_INCLUDED
#define QUARTIC_DEFORMER_HPP_INCLUDED

#include <deformer.hpp>
#include <vec3.hpp>

class quartic_deformer: public deformer{
public:
  //up = -1 to deform drum towards positive z
  quartic_deformer(double maxelong, double z_zero, double up, vec3 center);
  virtual double z(double x, double y) const override;

private:
  double maxelong;
  double z_zero;
  double up;
  point3 center; //drum center
};

#endif
