#ifndef CAMERA_HPP_INCLUDED
#define CAMERA_HPP_INCLUDED

#include <raytracing.hpp>

/*
The focus is at (0,0,0).
The camera screen is in the axis aligned plane through (0, 0, -focal_length)
The scene is in the negative-z domain.
Rays start at the focal point and flow through the camera pixels into the scene.
*/
class camera{
public:
  camera();
  camera(double aspect_ratio, double viewport_height, double viewport_width, double focal_length);
  ray get_ray(double u, double v) const;

private:
  point3 origin;
  point3 lower_left_corner;
  vec3 horizontal;
  vec3 vertical;
  int max_bounces; //maximum number of bounces cast rays can perform
};

#endif
