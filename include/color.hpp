#ifndef COLOR_HPP_INCLUDED
#define COLOR_HPP_INCLUDED

#include <vec3.hpp>
#include <iostream>

void write_color(std::ostream&, const color&, int samples_per_pixel);

#endif
