#ifndef TRIANGLE_MESH_HPP_INCLUDED
#define TRIANGLE_MESH_HPP_INCLUDED

#include <hittable.hpp>
#include <vec3.hpp>
#include <material.hpp>
#include <memory>
#include <string>
#include <triangle.hpp>
#include <deformer.hpp>

class triangle_mesh: public hittable{
public:
  //n_z_sign = sign that triangle normals z component should have
  triangle_mesh(std::string filename, std::shared_ptr<material> m, double n_z_sign, const deformer* fun);

  virtual bool hit(
    const ray& r, double t_min, double t_max, hit_record& rec) const override;

private:
  std::vector<triangle> triangles;
  std::string mesh_file;
};

#endif
