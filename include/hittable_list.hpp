#ifndef HITTABLE_LIST_HPP_INCLUDED
#define HITTABLE_LIST_HPP_INCLUDED

#include <hittable.hpp>
#include <memory>
#include <vector>

class hittable_list: public hittable{
public:
  hittable_list();
  hittable_list(std::shared_ptr<hittable> object);

  void clear();
  void add(std::shared_ptr<hittable> object);

  virtual bool hit(const ray& r, double t_min, double t_max, hit_record& rec) const override;

private:
  std::vector<std::shared_ptr<hittable> > objects;
};

#endif
