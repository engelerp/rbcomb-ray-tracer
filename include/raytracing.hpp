#ifndef RAYTRACING_HPP_INCLUDED
#define RAYTRACING_HPP_INCLUDED

#include <cmath>
#include <limits>
#include <memory>
#include <random>

/*Constants*/
const double infinity = std::numeric_limits<double>::infinity();
const double pi = 3.141592653589793238462643383279;

/*Utility*/
double degrees_to_radians(double degrees);
double random_double();
double random_double(double min, double max);
double clamp(double x, double min, double max);

/*Includes*/
#include <ray.hpp>
#include <vec3.hpp>
#include <color.hpp>

#endif
