#ifndef RADIATOR_HPP_INCLUDED
#define RADIATOR_HPP_INCLUDED

#include <material.hpp>
#include <vec3.hpp>

class radiator: public material{
public:
  radiator(int sourceID);

  virtual bool scatter(
    const ray& r_in, const hit_record& rec,
    double& fraction_reflected, ray& r_reflected,
    double& fraction_refracted, ray& r_refracted
  ) const override;

private:
  int sourceID;
};

#endif
