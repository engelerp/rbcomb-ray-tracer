import numpy as np
from .Utility import *
from .SymmetryOperation import *

class BoundaryMatching:
    def __init__(self, edge_A, edge_B):
        """Construct a BoundaryMatching object.

        Arguments:
        edge_1 -- [[xA0,yA0],[xA1,yA1]] list of two points defining edge_A
        edge_2 -- [[xB0,yB0],[xB1,yB1]] list of two points defining edge_B

        Notes:
        Points indexed '0' are mapped to the origin, points indexed '1' are
        mapped to the positive x axis.
        """
        self.pA0 = edge_A[0]
        self.pA1 = edge_A[1]
        self.rA = np.sqrt((self.pA1[0]-self.pA0[0])**2 + (self.pA1[1]-self.pA0[1])**2)
        self.pB0 = edge_B[0]
        self.pB1 = edge_B[1]
        self.rB = np.sqrt((self.pB1[0]-self.pB0[0])**2 + (self.pB1[1]-self.pB0[1])**2)
        #assertions
        if is_close(self.pA0[0],self.pA1[0]) and is_close(self.pA0[1],self.pA1[1]):
            raise RuntimeError("Short edge_A in BoundaryMatching")
        if is_close(self.pB0[0],self.pB1[0]) and is_close(self.pB0[1],self.pB1[1]):
            raise RuntimeError("Short edge_A in BoundaryMatching")

    def get_SymmetryOperations_A(self):
        """Get SymmetryOperations that map edge_A.

        Return:
        list of SymmetryOperations
        """
        #First we translate point0 to the origin
        op1 = SymmetryOperation(optype='translation', offset=[-self.pA0[0],-self.pA0[1]])
        p0 = [self.pA0[0]+op1.offset[0], self.pA0[1]+op1.offset[1]]
        p1 = [self.pA1[0]+op1.offset[0], self.pA1[1]+op1.offset[1]]
        #calculate rotation angle
        alpha = 0.
        #rotate into first quadrant
        while not(p1[0] >= 0. and p1[1] >= 0.):
            p1 = rotate(p1,-np.pi/2.)
            alpha -= np.pi/2.
        #add remaining rotation
        alpha -= np.arccos(p1[0]/self.rA)
        op2 = SymmetryOperation(optype='rotation', angle=alpha)
        return [op1,op2]


    def get_SymmetryOperations_B(self):
        """Get SymmetryOperations that map edge_B.

        Return:
        list of SymmetryOperations
        """
        #ingenious idea
        backup0 = copy.deepcopy(self.pA0)
        backup1 = copy.deepcopy(self.pA1)
        self.pA0 = copy.deepcopy(self.pB0)
        self.pA1 = copy.deepcopy(self.pB1)
        returnOps = self.get_SymmetryOperations_A()
        self.pA0 = copy.deepcopy(backup0)
        self.pA1 = copy.deepcopy(backup1)
        return returnOps
