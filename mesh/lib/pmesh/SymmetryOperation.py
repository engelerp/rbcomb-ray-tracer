#optype is one of 'rotation', 'mirror', 'translation', 'matrix'
#matrix provides access to matrix operations, with last column as translation vector for glide operations
# ( a11 a12 tx )
# ( a21 a22 ty )
import gzip
import copy
import numpy as np

from fenics import *
from mshr import *
import matplotlib.pyplot as plt

from .Utility import is_close

class SymmetryOperation:
    """Represents a symmetry operation.

    The available operations are rotations, mirrors, translations, and more general matrix transformations.
    It can generate its own inverse via the inverse member.
    """
    def __init__(self, optype, angle=0., rotation_center=[0.,0.], axis='x', mirror_angle=0., offset=[0.,0.], matrix=None):
        """Construct a SymmetryOperation.

        Arguments:
        optype -- type of the operation; 'rotation', 'mirror', 'translation' or 'matrix'
        angle -- for rotation: rotation angle, positive for CCW
        rotation_center -- for rotation: [x, y] the center of rotation
        axis -- for mirror: axis, either 'x' (x axis), 'y' (y axis) or 'c' (custom mirror_angle)
        mirror_angle - for mirror: angle between custom mirror axis and positive x-axis
        offset -- for translation: [x, y] translation vector
        matrix -- for matrix: 2x3 ([[[],[],[]],[[],[],[]]]) matrix, translation vector in last column
        """
        self.type = optype
        self.angle = angle
        self.center = rotation_center
        self.axis = axis
        self.mirror_angle = mirror_angle
        self.offset = offset
        self.matrix = matrix
        #handle custom mirror axes, these are implemented as matrices
        if self.type == 'mirror' and self.axis == 'c': #custom axis of norm 0
            self.type = 'matrix'
            s = np.sin(2.*self.mirror_angle)
            c = np.cos(2.*self.mirror_angle)
            self.matrix = [[    c,    s,    0.],
                           [    s,    -c,   0.]]

    def inverse(self):
        """Get the inverse SymmetryOperation.

        Return:
        SymmetryOperation op such that op*this = this*op = 1"""
        if self.type == 'rotation':
            return SymmetryOperation(self.type, angle=-self.angle, rotation_center=self.center, axis=self.axis, offset=self.offset, matrix=self.matrix)
        if self.type == 'mirror':
            return SymmetryOperation(self.type, angle=self.angle, rotation_center=self.center, axis=self.axis, offset=self.offset, matrix=self.matrix)
        if self.type == 'translation':
            return SymmetryOperation(self.type, angle=self.angle, rotation_center=self.center, axis=self.axis, offset=[-self.offset[0],-self.offset[1]], matrix=self.matrix)
        if self.type == 'matrix':
            det = self.matrix[0][0]*self.matrix[1][1] - self.matrix[0][1]*self.matrix[1][0]
            return SymmetryOperation(self.type, angle=self.angle, rotation_center=self.center, axis=self.axis, offset=self.offset, matrix=[[self.matrix[1][1]/det, -self.matrix[0][1]/det, -self.matrix[0][2]],[self.matrix[1][0]/det, self.matrix[0][0]/det, -self.matrix[1][2]]])

    def map(self, p):
        """Apply SymmetryOperation and return image.

        Arguments:
        p -- [x, y] point to map

        Return:
        q -- [x', y'] the image of p under SymmetryOperation
        """
        if self.type == 'rotation':
            c = np.cos(self.angle)
            s = np.sin(self.angle)
            return [c*(p[0]-self.center[0]) - s*(p[1]-self.center[1]) + self.center[0],
                    s*(p[0]-self.center[0]) + c*(p[1]-self.center[1]) + self.center[1]]
        elif self.type == 'mirror':
            if self.axis == 'x':
                return [p[0], -p[1]]
            elif self.axis == 'y':
                return [-p[0], p[1]]
            else:
                raise RuntimeError("SymmetryOperation.map: Invalid Mirror Axis.")

        elif self.type == 'translation':
            return [p[0] + self.offset[0], p[1] + self.offset[1]]

        elif self.type == 'matrix':
            return [self.matrix[0][0]*p[0] + self.matrix[0][1]*p[1] + self.matrix[0][2],
                    self.matrix[1][0]*p[0] + self.matrix[1][1]*p[1] + self.matrix[1][2]]

        else:
            raise RuntimeError("SymmetryOperation.map: Invalid SymmetryOperation.")
