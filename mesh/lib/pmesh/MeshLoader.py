import gzip
import copy
import numpy as np

from fenics import *
from mshr import *
import matplotlib.pyplot as plt

def load_mesh(path):
    """Load mesh from a .xml.gz file.

    Arguments:
    path -- path to the file, e.g. "./meshes/mesh.xml.gz"

    Return:
    dictionary that contains the mesh information
    """
    elements = {}
    with gzip.open(path,'r') as f:
        for bline in f:
        #for i in range(104):
        #    line = f.readline().decode('ascii')
            line = bline.decode('ascii') #decode line from binary to ascii
            #skip leading whitespace
            ch = 0
            while ch < len(line) and line[ch] != '<':
                ch += 1
            #skip '<'
            chb = ch+1
            #extract item name
            che = chb
            empty_tag = False
            while line[che] != ' ':
                if line[che] == '/' or line[che] == '>':
                    empty_tag = True
                    break
                che += 1
            if empty_tag:
                continue
            element = line[chb:che]
            if element not in elements:
                elements[element] = []
            elements[element].append({})
            while True:
                #skip whitespace
                ch = che
                while line[ch] == ' ':
                    ch += 1
                #can we extract another item
                if line[ch] != '/' and line[ch] != '>' and line[ch] != '?':
                    chb = ch
                    che = ch
                    while line[che] != '=':
                        che += 1
                    prop = line[chb:che]
                    #extract value
                    #skip '"'
                    chb = che+2
                    che = chb
                    while line[che] != '"':
                        che += 1
                    val = line[chb:che]
                    che += 1
                    elements[element][-1][prop] = val
                else:
                    break
    return elements

def print_elements(elements):
    """Print dictionary containing mesh information."""
    print(f"mesh type: {pmesh['mesh'][0]['celltype']}")
    print(f"mesh dimension: {pmesh['mesh'][0]['dim']}")
    print(f"vertices: ({pmesh['vertices'][0]['size']})")
    for v in pmesh['vertex']:
        print(f"\t{v['index']}\t{v['x']}\t{v['y']}")
    print(f"triangles: ({pmesh['cells'][0]['size']})")
    for t in pmesh['triangle']:
        print(f"\t{t['index']}\t{t['v0']}\t{t['v1']}\t{t['v2']}")
