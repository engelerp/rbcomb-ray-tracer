from .PMesh import *
from .SymmetryOperation import *
from .Utility import *
import numpy as np

def p4mm_mesh(lattice_constant, resolution=20):
    """Build a symmetrized p4mm PMesh.

    Arguments:
    lattice_constant -- desired lattice lattice constant
    resolution -- resolution of the mesh

    Return:
    PMesh

    Notes:
    Lattice vectors are aligned with the x and y axis
    """
    points = [[0.,0.], [lattice_constant/2., 0.], [lattice_constant/2., lattice_constant/2.]]
    fundamental_domain = Polygon([Point(x,y) for (x,y) in points])
    p4mm_mesh = generate_mesh(fundamental_domain, resolution)
    p4mm_pmesh = PMesh()
    p4mm_pmesh.from_mshr_Mesh(p4mm_mesh)
    refl45 = SymmetryOperation(optype='mirror', axis='c', mirror_angle=np.pi/4.)
    rot_90 = SymmetryOperation(optype='rotation', angle=-np.pi/2.)
    p4mm_pmesh.symmetrize([refl45,rot_90])
    p4mm_pmesh.match_boundary_vertices(
    [
    SymmetryOperation(optype='rotation', angle=np.pi/2.),
    SymmetryOperation(optype='translation', offset=[lattice_constant/2.,-lattice_constant/2.])],
    [
    SymmetryOperation(optype='rotation', angle=-np.pi/2.),
    SymmetryOperation(optype='translation', offset=[lattice_constant/2.,lattice_constant/2.])]
    )
    #top and bottom
    p4mm_pmesh.match_boundary_vertices(
    [
    SymmetryOperation(optype='translation', offset=[lattice_constant/2.,-lattice_constant/2.])],
    [
    SymmetryOperation(optype='translation', offset=[lattice_constant/2.,lattice_constant/2.])]
    )
    return p4mm_pmesh
