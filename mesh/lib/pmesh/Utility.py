import gzip
import copy
import numpy as np
from scipy.sparse import dok_matrix

from fenics import *
from mshr import *
import matplotlib.pyplot as plt
import math

#Global variables
"""Tolerance for is_close comparisons between numbers"""
ISCLOSE_ABS_TOL = 1e-5


# DEPRECATED: Use a direct call to built-in math.isclose instead:
# math.isclose(val,targ,abstol=tol)
def is_close(val, targ, tol=1e-5):
    """Check if two numbers are within a certain tolerance from each other.

    Arguments:
    val -- number to be checked
    targ -- number to be checked
    tol -- maximum distance between the numbers

    Return:
    True if numbers are closer than tol, False else
    """
    #return (np.abs(val-targ) < tol)
    #return (abs(val-targ) < tol)
    return math.isclose(val, targ)

#rotate CCW around origin
def rotate(coords, angle):
    """Get image of a point under rotation around the origin.

    Arguments:
    coords -- [x, y] point to rotate, is not modified
    angle -- angle (rad) to rotate by, positive for CCW

    Return:
    [x', y'] rotated point
    """
    r = np.sqrt(coords[0]*coords[0]+coords[1]*coords[1])
    c = np.cos(angle)
    s = np.sin(angle)
    x = c*coords[0] - s*coords[1]
    y = s*coords[0] + c*coords[1]
    return [x, y]

#this only works if all boundary is covered by pbcs
def in_interior(v,pbcs):
    """Check if a vertex lies in the interior of a mesh.

    Arguments:
    v -- [x,y] the vertex coordinates to check
    pbcs -- PeriodicBoundaries that are applied to the boundaries

    Return:
    True if point is in interior, False if point is on boundary

    Notes:
    To generate reduced coordinates (remove vertices on boundary), use like
    coords = Vz.tabulate_dof_coordinates()
    reduced = [[c[0],c[1]] for c in coords[::2] if in_interior(c,pbcs)]"""
    if pbcs.inside(v,True) or pbcs.insideH(v,True):
        return False
    return True

def symmetryToMatrix(coords, op):
    """Calculate matrix corresponding to a symmetry operation.

    Arguments:
    coords -- [[x0,y0],[x1,y1],...] list of vertex coordinates
    op -- SymmetryOperation of which the matrix representation is desired

    Return:
    scipy.sparse.dok_matrix m with m[i,j]==1 iff op(coords[i]) == coords[j]

    Notes:
    Throws RuntimeError if op doesn't map coords->coords."""
    mat = dok_matrix((len(coords), len(coords)), dtype=np.float32)
    transformed_coords = [op.map(p) for p in coords]
    #for i, p0 in enumerate(coords):
    #    q = op.map(p0)
    for i,[q0,q1] in enumerate(transformed_coords):
        found_image = False
        for j, [p0,p1] in enumerate(coords):
            #if is_close(p1[0],q[0]) and is_close(p1[1],q[1]):
            #if (abs(p0-q0) < ISCLOSE_ABS_TOL) and (abs(p1-q1) < ISCLOSE_ABS_TOL):
            if (math.isclose(p0,q0,abs_tol=ISCLOSE_ABS_TOL)) and (math.isclose(p1,q1,abs_tol=ISCLOSE_ABS_TOL)):
                mat[i,j] = 1
                found_image = True
                break
        if not found_image:
            raise RuntimeError(f"symmetryToMatrix: Image ({q0},{q1}) not found")
    return mat
