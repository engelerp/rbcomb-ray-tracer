#need to set v0, v1 (vertices on boundary), vt (tip vertex), t (corresponding element)
import gzip
import copy
import numpy as np

from fenics import *
from mshr import *
import matplotlib.pyplot as plt

from .Utility import is_close, rotate
import math

#Global variables
"""Tolerance for is_close comparisons between numbers"""
ISCLOSE_ABS_TOL = 1e-5


class BoundaryInterval:
    """Represents a horizontal interval with associated triangle.

    It contains two vertices v1, v2 that lie on a horizontal line and define an
    interval between them, along with a third vertex vt that lies off the line.
    These three vertices span a triangle element, t. The BoundaryInterval can
    check if a point lies within its interval, can add points to its interval and
    generate new elements to replace t, such that the added points can be added
    to the mesh as vertices."""
    def __init__(self, v0, v1, vt, t):
        """Construct a BoundaryInterval.

        Arguments:
        v0 -- vertex at left boundary of interval (small x coordinate)
        v1 -- vertex at right boundary of interval (big x coordinate)
        vt -- vertex that lies off the line, tip vertex
        t -- triangle element associated with vertices v0, v1, vt
        """
        self.v0 = v0
        self.v1 = v1
        self.vt = vt
        self.t = t
        self.interior_vertices = []

    #workflow: check if v in interval. if yes, add to vertices, then add vertices[-1] to interval.
    def is_in_interval(self, v):
        """Check if a vertex lies in the interior of the interval.

        Arguments:
        v -- vertex to be checked

        Return:
        True if the vertex is in the interior of the interval, False else
        """
        #all vertices are on the y=0 line
        #if not is_close(v['y'],self.v0['y']):
        #if not (abs(v['y']-self.v0['y']) < ISCLOSE_ABS_TOL):
        if not math.isclose(v['y'],self.v0['y'],abs_tol=ISCLOSE_ABS_TOL):
            #print(f"({self.v0['x']}, {self.v0['y']})\t({self.v1['x']}, {self.v1['y']})\t({v['x']}, {v['y']})\tOff on y-axis")
            return False
        #can't be on the boundary, needs to be in the interior
        #if ( (is_close(v['x'], self.v0['x']) and is_close(v['y'], self.v0['y'])) or (is_close(v['x'], self.v1['x']) and is_close(v['y'], self.v1['y'])) ):
        #if ( ((abs(v['x']-self.v0['x']) < ISCLOSE_ABS_TOL) and (abs(v['y']-self.v0['y']) < ISCLOSE_ABS_TOL)) or ((abs(v['x']-self.v1['x']) < ISCLOSE_ABS_TOL) and (abs(v['y']-self.v1['y']) < ISCLOSE_ABS_TOL))):
        if ( (math.isclose(v['x'], self.v0['x'],abs_tol=ISCLOSE_ABS_TOL) and math.isclose(v['y'], self.v0['y'],abs_tol=ISCLOSE_ABS_TOL)) or (math.isclose(v['x'], self.v1['x'],abs_tol=ISCLOSE_ABS_TOL) and math.isclose(v['y'], self.v1['y'],abs_tol=ISCLOSE_ABS_TOL)) ):
            #print(f"({self.v0['x']}, {self.v0['y']})\t({self.v1['x']}, {self.v1['y']})\t({v['x']}, {v['y']})\tOn boundary")
            return False
        #is between the boundaries
        if self.v0['x'] < v['x'] and v['x'] < self.v1['x']:
            #print(f"({self.v0['x']}, {self.v0['y']})\t({self.v1['x']}, {self.v1['y']})\t({v['x']}, {v['y']})\tIs in between")
            #print(f"{self.v0['x']}\t{v['x']}\t{self.v1['x']}")
            return True
        #is outside the boundaries
        else:
            #print(f"({self.v0['x']}, {self.v0['y']})\t({self.v1['x']}, {self.v1['y']})\t({v['x']}, {v['y']})\tNot in between")
            return False

    def add_interior_vertex(self, v):
        """Add vertex to list of interior vertices.

        Arguments:
        v -- vertex to be added

        Notes:
        The user is responsible for ensuring the vertex lies in the interval.
        This can be achieved by calling is_in_interval.
        """
        self.interior_vertices.append(v)

    def get_elements(self):
        """Get triangle elements that replace t.

        Return:
        list of triangle elements

        Notes:
        This function returns the elements that need to be added to a mesh for
        addition of the vertices listed in interior_vertices. The returned
        elements cover t, i.e. t can be deleted when the new elements are added.
        Note that all returned elements have index 0, the user needs to rebalance
        indices once the elements are added to the mesh. The vertex indices are
        the same as those of the vertices in the interior_vertices, v0, v1 and vt.
        """
        elements = []
        v_prev = self.v0
        for v in self.interior_vertices:
            elements.append({})
            elements[-1]['index'] = 0
            elements[-1]['v0'] = v_prev['index']
            elements[-1]['v1'] = v['index']
            elements[-1]['v2'] = self.vt['index']
            v_prev = v
        elements.append({})
        elements[-1]['index'] = 0
        elements[-1]['v0'] = v_prev['index']
        elements[-1]['v1'] = self.v1['index']
        elements[-1]['v2'] = self.vt['index']
        return elements

    def element_needs_removal(self):
        """Check if t needs to be replaced.

        Return:
        True if t needs to be replaced, False else.

        Notes: t needs to be replaced (refined) if there is at least one interior
        vertex."""
        return len(self.interior_vertices) != 0

    def print_interval(self):
        """Print BoundaryInterval."""
        print(f"Interval ({self.v0['x']}, {self.v0['y']})\t({self.v1['x']}, {self.v1['y']})\tanchor ({self.vt['x']}, {self.vt['y']})")

    def __str__(self):
        """Convert BoundaryInterval to string."""
        return f"Interval ({self.v0['x']}, {self.v0['y']})\t({self.v1['x']}, {self.v1['y']})\t({self.vt['x']}, {self.vt['y']})"
