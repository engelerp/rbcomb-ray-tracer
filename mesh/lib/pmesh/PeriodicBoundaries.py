from .BoundaryMatching import *
from fenics import *
from math import isclose
#from .Utility import is_close

class PeriodicBoundaries(SubDomain):
    """Class to define a set of periodic boundaries.

    This follows the same naming convention as the dolfin example:
    https://fenicsproject.org/olddocs/dolfin/1.4.0/python/demo/documented/periodic/python/documentation.html
    The periodic boundary is split into two domains: the target domain G and the
    map domain H. Then every point in H is mapped to some point in G.

    For now, it only supports translational periodic boundary conditions, i.e.
    the map between two edges of an edge pair is a mere translation. Maps for
    different edge pairs may be different."""
    def __init__(self, **kwargs):
        """Construct a PeriodicBoundaries object."""
        super(PeriodicBoundaries, self).__init__(**kwargs)
        self.pointsG = []
        self.pointsH = []
        self.point_translations = []
        self.edgesG_info = []
        self.edgesH_info = []
        self.edge_translations = []

    def add_point_constraint(self, pH, pG):
        """Add a point map to the periodic boundary.

        Arguments:
        pH -- [x,y] map domain point in H
        pG -- [x,y] target point in G
        """
        self.pointsG.append(pG)
        self.pointsH.append(pH)
        self.point_translations.append([pG[0]-pH[0], pG[1]-pH[1]])

    def add_edge_constraint(self, eH, eG):
        """Add an edge map to the periodic boundary.

        Arguments:
        eH -- [[xH0, yH0], [xH1,yH1]] edge in H defined through its end points
        eG -- [[xG0, yG0], [xG1,yG1]] edge in G defined through its end points

        Notes:
        The character of the induced linear map m: H->G is such that on the
        corners, the map would be
        m(pH0) = pG0, m(pH1) = pG1,
        except that it only maps the interior of the edges. Corners must be
        added manually via PeriodicBoundaries.add_point_constraint.
        This means the two edges must be the same up to a translation.
        """
        self.edge_translations.append([eG[0][0]-eH[0][0], eG[0][1]-eH[0][1]])
        horizontal = False
        vertical = False
        if math.isclose(eG[0][0],eG[1][0],abs_tol=ISCLOSE_ABS_TOL):
            vertical = True
        elif math.isclose(eG[0][1],eG[1][1],abs_tol=ISCLOSE_ABS_TOL):
            horizontal = True
        if not vertical and not horizontal:
            self.edgesG_info.append([eG[0], [1./(eG[1][0]-eG[0][0]),1./(eG[1][1]-eG[0][1])]])
            self.edgesH_info.append([eH[0], [1./(eH[1][0]-eH[0][0]),1./(eH[1][1]-eH[0][1])]])
        #if the line is vertical, we store [eX[0], ['vertical', eX[1][1]]]
        elif vertical:
            self.edgesG_info.append([eG[0], ['vertical',eG[1][1]]])
            self.edgesH_info.append([eH[0], ['vertical',eH[1][1]]])
        #if the line is horizontal, we store [eX[0], ['horizontal', eX[1][0]]]
        else:
            self.edgesG_info.append([eG[0], ['horizontal',eG[1][0]]])
            self.edgesH_info.append([eH[0], ['horizontal',eH[1][0]]])

    def inside(self, x, on_boundary):
        """Check if a point lies within G"""
        if not on_boundary:
            return False
        #edges
        for e in self.edgesG_info:
            if not e[1][0] in ['horizontal','vertical']:
                tx = (x[0]-e[0][0])*e[1][0]
                ty = (x[1]-e[0][1])*e[1][1]
                if math.isclose(tx,ty,abs_tol=ISCLOSE_ABS_TOL) and tx > 0. and tx < 1. and not math.isclose(tx,1.,abs_tol=ISCLOSE_ABS_TOL) and not math.isclose(tx,0.,abs_tol=ISCLOSE_ABS_TOL):
                    #print(f"General {x[0]}, {x[1]}")
                    return True
            #vertical line
            elif e[1][0] == 'vertical':
                on_edge = math.isclose(x[0],e[0][0],abs_tol=ISCLOSE_ABS_TOL)
                on_not_corner = not (math.isclose(x[1],e[0][1],abs_tol=ISCLOSE_ABS_TOL) or math.isclose(x[1],e[1][1],abs_tol=ISCLOSE_ABS_TOL))
                on_interior = (x[1] < e[0][1] and e[1][1] < x[1]) or (x[1] < e[1][1] and e[0][1] < x[1])
                if on_edge and on_not_corner and on_interior:
                    #print(f"Vertical {x[0]}, {x[1]}")
                    return True
            #horizontal edge
            else:
                on_edge = math.isclose(x[1],e[0][1],abs_tol=ISCLOSE_ABS_TOL)
                on_not_corner = not (math.isclose(x[0],e[0][0],abs_tol=ISCLOSE_ABS_TOL) or math.isclose(x[0],e[1][1],abs_tol=ISCLOSE_ABS_TOL))
                on_interior = (x[0] < e[0][0] and e[1][1] < x[0]) or (x[0] < e[1][1] and e[0][0] < x[0])
                if on_edge and on_not_corner and on_interior:
                    #print(f"Horizontal {x[0]}, {x[1]}")
                    return True
        #points
        for p in self.pointsG:
            if math.isclose(p[0],x[0],abs_tol=ISCLOSE_ABS_TOL) and math.isclose(p[1],x[1],abs_tol=ISCLOSE_ABS_TOL):
                #print(f"Point {x[0]}, {x[1]}")
                return True
        return False

    def map(self, x, y):
        """Map a point in H to its image in G"""
        for i,e in enumerate(self.edgesH_info):
            if not e[1][0] in ['horizontal','vertical']:
                tx = (x[0]-e[0][0])*e[1][0]
                ty = (x[1]-e[0][1])*e[1][1]
                if math.isclose(tx,ty,abs_tol=ISCLOSE_ABS_TOL) and tx > 0. and tx < 1. and not math.isclose(tx,1.,abs_tol=ISCLOSE_ABS_TOL) and not math.isclose(tx,0.,abs_tol=ISCLOSE_ABS_TOL):
                    y[0] = x[0] + self.edge_translations[i][0]
                    y[1] = x[1] + self.edge_translations[i][1]
                    return
            #vertical line
            elif e[1][0] == 'vertical':
                on_edge = math.isclose(x[0],e[0][0],abs_tol=ISCLOSE_ABS_TOL)
                on_not_corner = not (math.isclose(x[1],e[0][1],abs_tol=ISCLOSE_ABS_TOL) or math.isclose(x[1],e[1][1],abs_tol=ISCLOSE_ABS_TOL))
                on_interior = (x[1] < e[0][1] and e[1][1] < x[1]) or (x[1] < e[1][1] and e[0][1] < x[1])
                if on_edge and on_not_corner and on_interior:
                    y[0] = x[0] + self.edge_translations[i][0]
                    y[1] = x[1] + self.edge_translations[i][1]
                    return
            #horizontal edge
            else:
                on_edge = math.isclose(x[1],e[0][1],abs_tol=ISCLOSE_ABS_TOL)
                on_not_corner = not (math.isclose(x[0],e[0][0],abs_tol=1e-5) or math.isclose(x[0],e[1][1],abs_tol=1e-5))
                on_interior = (x[0] < e[0][0] and e[1][1] < x[0]) or (x[0] < e[1][1] and e[0][0] < x[0])
                if on_edge and on_not_corner and on_interior:
                    y[0] = x[0] + self.edge_translations[i][0]
                    y[1] = x[1] + self.edge_translations[i][1]
                    return
        #points
        for i,p in enumerate(self.pointsH):
            if math.isclose(p[0],x[0],abs_tol=ISCLOSE_ABS_TOL) and math.isclose(p[1],x[1],abs_tol=ISCLOSE_ABS_TOL):
                y[0] = x[0] + self.point_translations[i][0]
                y[1] = x[1] + self.point_translations[i][1]
                return
        #this point should never be reached
        print(f"Mapping failed on [{x[0]},{x[1]}]")
        y[0] = 1000.
        y[1] = 1000.
        #raise RuntimeError(f"PeriodicBoundaries.map: Unable to map point [{x[0]}, {x[1]}]")

    def insideH(self, x, on_boundary):
        """Check if a point lies within H"""
        if not on_boundary:
            return False
        #edges
        for e in self.edgesH_info:
            if not e[1][0] in ['horizontal','vertical']:
                tx = (x[0]-e[0][0])*e[1][0]
                ty = (x[1]-e[0][1])*e[1][1]
                if math.isclose(tx,ty,abs_tol=ISCLOSE_ABS_TOL) and tx > 0. and tx < 1. and not math.isclose(tx,1.,abs_tol=ISCLOSE_ABS_TOL) and not math.isclose(tx,0.,abs_tol=ISCLOSE_ABS_TOL):
                    #print(f"General {x[0]}, {x[1]}")
                    return True
            #vertical line
            elif e[1][0] == 'vertical':
                on_edge = math.isclose(x[0],e[0][0],abs_tol=ISCLOSE_ABS_TOL)
                on_not_corner = not (math.isclose(x[1],e[0][1],abs_tol=ISCLOSE_ABS_TOL) or math.isclose(x[1],e[1][1],abs_tol=ISCLOSE_ABS_TOL))
                on_interior = (x[1] < e[0][1] and e[1][1] < x[1]) or (x[1] < e[1][1] and e[0][1] < x[1])
                if on_edge and on_not_corner and on_interior:
                    #print(f"Vertical {x[0]}, {x[1]}")
                    return True
            #horizontal edge
            else:
                on_edge = math.isclose(x[1],e[0][1],abs_tol=ISCLOSE_ABS_TOL)
                on_not_corner = not (math.isclose(x[0],e[0][0],abs_tol=ISCLOSE_ABS_TOL) or math.isclose(x[0],e[1][1],abs_tol=ISCLOSE_ABS_TOL))
                on_interior = (x[0] < e[0][0] and e[1][1] < x[0]) or (x[0] < e[1][1] and e[0][0] < x[0])
                if on_edge and on_not_corner and on_interior:
                    #print(f"Horizontal {x[0]}, {x[1]}")
                    return True
        #points
        for p in self.pointsH:
            if math.isclose(p[0],x[0],abs_tol=ISCLOSE_ABS_TOL) and math.isclose(p[1],x[1],abs_tol=ISCLOSE_ABS_TOL):
                #print(f"Point {x[0]}, {x[1]}")
                return True
        return False
