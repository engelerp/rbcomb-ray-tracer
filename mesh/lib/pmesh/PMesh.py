from .MeshLoader import load_mesh
from .SymmetryOperation import SymmetryOperation
from .BoundaryInterval import BoundaryInterval
from .Utility import is_close, rotate
from .BoundaryMatching import BoundaryMatching

import gzip
import copy
import numpy as np
from scipy.sparse import dok_matrix

from fenics import *
from mshr import *
import matplotlib.pyplot as plt
import math

#Global variables
"""Tolerance for is_close comparisons between numbers"""
ISCLOSE_ABS_TOL = 1e-5

class PMesh:
    """Representation of a mesh with useful operations.

    More elaborate description is here.
    """

    def __init__(self):
        """Default construct an empty mesh."""
        self.vertices = []
        self.triangles = []
        self.dimension = 2
        self.mesh_type = "triangle"

    def load_elements(self, elements):
        """Initialize mesh from a mesh dictionary

        Arguments:
        elements -- a mesh dictionary
        """
        self.dimension = int(elements['mesh'][0]['dim'])
        self.cell_type = elements['mesh'][0]['celltype']
        #clear the mesh
        self.vertices = []
        self.triangles = []
        for v in elements['vertex']:
            self.vertices.append({})
            self.vertices[-1]['index'] = int(v['index'])
            self.vertices[-1]['x'] = float(v['x'])
            self.vertices[-1]['y'] = float(v['y'])
        for t in elements['triangle']:
            self.triangles.append({})
            self.triangles[-1]['index'] = int(t['index'])
            self.triangles[-1]['v0'] = int(t['v0'])
            self.triangles[-1]['v1'] = int(t['v1'])
            self.triangles[-1]['v2'] = int(t['v2'])

    def print(self):
        """Print the mesh."""
        print(f"mesh type: {self.mesh_type}")
        print(f"mesh dimension: {self.dimension}")
        print(f"vertices: ({len(self.vertices)})")
        for v in self.vertices:
            print(f"\t{v['index']}\t{v['x']}\t{v['y']}")
        print(f"triangles: ({len(self.triangles)})")
        for t in self.triangles:
            print(f"\t{t['index']}\t{t['v0']}\t{t['v1']}\t{t['v2']}")

    #OBSOLETE: replaced by symmetrize_operation
    def mirror_x_symmetrize(self):
        """Symmetrize the mesh by mirroring it on the x axis.

        It is assumed that all vertices have y>=0 before this function is called.
        The mesh size is about doubled in this process.
        """
        #find original vertices in boundary
        boundary_indices = []
        for v in self.vertices:
            #if is_close(v['y'], 0.):
            #if (abs(v['y']) < ISCLOSE_ABS_TOL):
            if math.isclose(v['y'], 0., abs_tol=ISCLOSE_ABS_TOL):
                boundary_indices.append(v['index'])
        #generate mirrored vertices
        new_vertices = []
        for v in self.vertices:
            if v['index'] not in boundary_indices:
                new_vertices.append(copy.copy(v))
                new_vertices[-1]['index'] += len(self.vertices)
                new_vertices[-1]['y'] = -v['y']
        #generate mirrored connectivity
        new_triangles = []
        for t in self.triangles:
            new_triangles.append(copy.copy(t))
            index = t['index']
            v0 = t['v0']
            v1 = t['v1']
            v2 = t['v2']
            new_triangles[-1]['index'] = index + len(self.triangles)
            if v0 not in boundary_indices:
                v0 += len(self.vertices)
            if v1 not in boundary_indices:
                v1 += len(self.vertices)
            if v2 not in boundary_indices:
                v2 += len(self.vertices)
            new_triangles[-1]['v0'] = v0
            new_triangles[-1]['v1'] = v1
            new_triangles[-1]['v2'] = v2
        #append new mesh to old mesh
        for v in new_vertices:
            self.vertices.append(v)
        for t in new_triangles:
            self.triangles.append(t)

        #clean up indices, else dolfin import fails
        index_mapping = {}
        for i in range(len(self.vertices)):
            if self.vertices[i]['index'] != i:
                index_mapping[self.vertices[i]['index']] = i
                self.vertices[i]['index'] = i
        #fix triangle indexing
        for index in index_mapping.keys():
            for t in self.triangles:
                if t['v0'] == index:
                    t['v0'] = index_mapping[index]
                if t['v1'] == index:
                    t['v1'] = index_mapping[index]
                if t['v2'] == index:
                    t['v2'] = index_mapping[index]

    def append_mesh(self, mesh):
        """Append another PMesh to the current mesh.

        Arguments:
        mesh -- PMesh to append

        Return:
        True if vertices were appended, False else

        Notes:
        Only vertices that don't exist yet in the current mesh will be appended.
        """
        #discover common vertices to find vertex mapping
        index_map = {}
        deleted_vertices = []
        for v in mesh.vertices:
            for w in self.vertices:
                #if is_close(v['x'],w['x']) and is_close(v['y'],w['y']):#same vertex
                #if (abs(v['x']-w['x']) < ISCLOSE_ABS_TOL) and (abs(v['y']-w['y']) < ISCLOSE_ABS_TOL):
                if math.isclose(v['x'],w['x'],abs_tol=ISCLOSE_ABS_TOL) and math.isclose(v['y'],w['y'],abs_tol=ISCLOSE_ABS_TOL):#same vertex
                    index_map[v['index']] = w['index']
                    deleted_vertices.append(v['index'])
                    break #there should maximally be one of these w's per v
        if len(index_map.keys()) == len(mesh.vertices): #nothing will be appended
            return False
        #append mesh.vertices to vertices
        appended = False
        for v in mesh.vertices:
            if not v['index'] in index_map.keys(): #we don't append mapped vertices
                appended = True
                self.vertices.append(copy.copy(v))
                self.vertices[-1]['index'] = len(self.vertices)-1
                #complete the vertex mapping
                index_map[v['index']] = self.vertices[-1]['index']
        #append mesh.triangles to triangles
        for t in mesh.triangles:
            #if all vertices of the mesh were deleted, we skip the triangle
            #i believe it's then probable that the meshes fully overlap
            if t['v0'] in deleted_vertices and t['v1'] in deleted_vertices and t['v2'] in deleted_vertices:
                continue
            self.triangles.append(copy.copy(t))
            self.triangles[-1]['index'] = len(self.triangles)-1
            #update vertex indices according to mapping
            for vertex in ['v0','v1','v2']:
                self.triangles[-1][vertex] = index_map[self.triangles[-1][vertex]]
        return appended

    def symmetrize_operation(self, op):
        """Symmetrize the mesh by applying a SymmetryOperation.

        Arguments:
        op -- SymmetryOperation that is to be applied

        Notes:
        Applies op to the mesh to generate a new mesh m_new from the original m_orig.
        The meshes m_new and m_orig must have exactly a common boundary and not overlap.
        These two meshes are then concatenated to obtain a mesh that is symmetrical
        under the operation op.
        """
        #generate new mesh by copying
        m_new = copy.deepcopy(self)
        #transform new mesh
        m_new.apply_symmetry_operation(op)
        #append new mesh
        self.append_mesh(m_new)

    def symmetrize(self, ops):
        """Generate mesh by applying SymmetryOperations.

        Arguments:
        ops -- list of symmetry operations

        Notes:
        The mesh is successively symmetrized to each of the operations in ops.
        Each operation in ops must be a root of unity.
        """
        for op in ops:
            if isinstance(op, BoundaryMatching):
                self.match_boundary_vertices(op.get_SymmetryOperations_A(), op.get_SymmetryOperations_B())
            elif isinstance(op, SymmetryOperation):
                #copy current mesh
                opmesh = copy.deepcopy(self)
                opmesh.apply_symmetry_operation(op)
                while(self.append_mesh(opmesh)):
                    opmesh.apply_symmetry_operation(op)
            else:
                raise RuntimeError("Unknown operation encountered in PMesh.symmetrize")

    def declare_basis(self, angle):
        """Declare the current mesh as fundamental domain.

        Arguments:
        angle -- the rotation angle (rad) used to generate the unit cell.

        Notes:
        The mesh must be placed such that it has a boundary on the positive x axis
        and lies in the y>=0 half plane.
        The basis will be rotated to lie in the y<=0 half plane, with a boundary
        on the x-axis.
        """
        #copy vertices
        self.basis_vertices = []
        for v in self.vertices:
            self.basis_vertices.append(copy.copy(v))
        #copy triangles
        self.basis_triangles = []
        for t in self.triangles:
            self.basis_triangles.append(copy.copy(t))
        #find lower boundary vertex indices
        self.boundary_indices_lower = []
        for v in self.basis_vertices:
            #if is_close(v['y'], 0.):
            #if (abs(v['y']) < ISCLOSE_ABS_TOL):
            if math.isclose(v['y'], 0., abs_tol=ISCLOSE_ABS_TOL):
                self.boundary_indices_lower.append(v['index'])
        #rotate basis into place
        for v in self.basis_vertices:
            new_coords = rotate([v['x'],v['y']], -angle)
            v['x'] = new_coords[0]
            v['y'] = new_coords[1]
        #identify upper boundary vertex indices
        self.boundary_indices_upper = []
        for v in self.basis_vertices:
            #if is_close(v['y'], 0.):
            #if (abs(v['y']) < ISCLOSE_ABS_TOL):
            if math.isclose(v['y'], 0., abs_tol=ISCLOSE_ABS_TOL):
                self.boundary_indices_upper.append(v['index'])

    def append_basis(self, close_loop=False):
        """Append a fundamental domain slice to the mesh.

        Arguments:
        close_loop -- True if the funcamental domain needs to be matched on both
        sides, False else.
        """
        #find lower boundary indices of structure and find their mapping
        boundary_indices = []
        index_mapping = {}
        for v in self.vertices:
            #if is_close(v['y'], 0.):
            #if (abs(v['y']) < ISCLOSE_ABS_TOL):
            if math.isclose(v['y'], 0., abs_tol=ISCLOSE_ABS_TOL):
                boundary_indices.append(v['index'])
                #search the corresponding index in the basis
                for w in self.basis_vertices:
                    #if is_close(w['x'],v['x']) and is_close(v['y'], w['y']):
                    #if (abs(w['x']-v['x']) < ISCLOSE_ABS_TOL) and (abs(v['y']-w['y']) < ISCLOSE_ABS_TOL):
                    if math.isclose(w['x'],v['x'],abs_tol=ISCLOSE_ABS_TOL) and math.isclose(v['y'], w['y'], abs_tol=ISCLOSE_ABS_TOL):
                        index_mapping[w['index']] = v['index']
            elif v['index'] in self.boundary_indices_upper:
                for w in self.basis_vertices:
                    #if is_close(w['x'],v['x']) and is_close(v['y'],w['y']):
                    #if (abs(w['x']-v['x']) < ISCLOSE_ABS_TOL) and (abs(v['y']-w['y']) < ISCLOSE_ABS_TOL):
                    if math.isclose(w['x'],v['x'],abs_tol=ISCLOSE_ABS_TOL) and math.isclose(v['y'],w['y'],abs_tol=ISCLOSE_ABS_TOL):
                        index_mapping[w['index']] = v['index']
        #copy new vertices
        new_vertices = []
        current_index = 0
        for v in self.basis_vertices:
            #we don't need to re-place the boundary vertices, they are already in the old mesh
            if close_loop and v['index'] in self.boundary_indices_lower:
                continue
            if not v['index'] in self.boundary_indices_upper:
                new_vertices.append(copy.copy(v))
                index_mapping[new_vertices[-1]['index']] = current_index + len(self.vertices)
                new_vertices[-1]['index'] = current_index + len(self.vertices)
                current_index += 1
        #copy new triangles
        new_triangles = []
        for i in range(len(self.basis_triangles)):
            new_triangles.append(copy.copy(self.basis_triangles[i]))
            new_triangles[-1]['index'] = i + len(self.triangles)
            new_triangles[-1]['v0'] = index_mapping[new_triangles[-1]['v0']]
            new_triangles[-1]['v1'] = index_mapping[new_triangles[-1]['v1']]
            new_triangles[-1]['v2'] = index_mapping[new_triangles[-1]['v2']]
        #copy vertices and triangles over
        for v in new_vertices:
            self.vertices.append(copy.copy(v))
        for t in new_triangles:
            self.triangles.append(copy.copy(t))

    def translate_mesh(self, offset):
        """Translate the entire mesh.

        Arguments:
        offset -- [x, y] the vector by which the mesh is translated
        """
        for v in self.vertices:
            v['x'] += offset[0]
            v['y'] += offset[1]

    def rotate_mesh(self, angle, center=[0.,0.]):
        """Rotate the entire mesh.

        Arguments:
        angle -- rotation angle (rad), positive for CCW rotations
        center -- [x,y] center of rotation
        """
        self.translate_mesh([-center[0],-center[1]])
        for v in self.vertices:
            new_xy = rotate([v['x'],v['y']] ,angle)
            v['x'] = new_xy[0]
            v['y'] = new_xy[1]
        self.translate_mesh([center[0],center[1]])

    #axis is either 'x' or 'y'
    def mirror_mesh(self, axis):
        """Mirror the entire mesh.

        Arguments:
        axis -- 'x' or 'y', the axis on which to mirror
        """
        if axis == 'x':
            for v in self.vertices:
                v['y'] = -v['y']
        elif axis == 'y':
            for v in self.vertices:
                v['x'] = -v['x']

    def apply_matrix(self, matrix):
        """Apply a 2x3 transformation matrix to each mesh vertex.

        Arguments:
        matrix -- transformation matrix, translation vector in third column,
        row major ordering.
        """
        for v in self.vertices:
            x = matrix[0][0]*v['x'] + matrix[0][1]*v['y'] + matrix[0][2]
            y = matrix[1][0]*v['x'] + matrix[1][1]*v['y'] + matrix[0][2]
            v['x'] = x
            v['y'] = y

    def apply_symmetry_operation(self, op):
        """Apply a SymmetryOperation to the mesh.

        Arguments:
        op -- SymmetryOperation to be applied
        """
        if op.type == 'rotation':
            self.rotate_mesh(op.angle, op.center)
        elif op.type == 'mirror':
            self.mirror_mesh(op.axis)
        elif op.type == 'translation':
            self.translate_mesh(op.offset)
        elif op.type == 'matrix':
            self.apply_matrix(op.matrix)

    def extract_xaxis_vertices(self):
        """Get sorted list of all vertices that lie on the x axis.

        Return:
        list of vertices sorted by increasing x coordinate
        """
        bv1 = []
        for v in self.vertices:
            #if is_close(v['y'],0.):
            #if (abs(v['y']) < ISCLOSE_ABS_TOL):
            if math.isclose(v['y'],0.,abs_tol=ISCLOSE_ABS_TOL):
                bv1.append(copy.copy(v))
        bv1.sort(key=lambda v: v['x'])
        return bv1

    def extract_boundary_intervals(self, boundary_vertices):
        """Convert a sorted list of vertices on the x axis to a list of BoundaryIntervals.

        Arguments:
        boundary_vertices -- list of vertices on x axis sorted by increasing x coordinate

        Return:
        list of BoundaryIntervals covering the vertices from left to right
        """
        bv1 = boundary_vertices
        intervals_0 = []
        for i in range(len(bv1)-1):
            v1 = bv1[i]['index']
            v2 = bv1[i+1]['index']
            for t in self.triangles:
                trianglevertices0 = [t['v0'],t['v1']]
                trianglevertices1 = [t['v0'],t['v2']]
                trianglevertices2 = [t['v2'],t['v1']]
                if v1 in trianglevertices0 and v2 in trianglevertices0:
                    interv = BoundaryInterval(copy.copy(bv1[i]),copy.copy(bv1[i+1]),copy.copy(self.vertices[t['v2']]),copy.copy(t))
                    intervals_0.append(copy.copy(interv))
                    #print(intervals_0[-1])
                    break
                elif v1 in trianglevertices1 and v2 in trianglevertices1:
                    interv = BoundaryInterval(copy.copy(bv1[i]),copy.copy(bv1[i+1]),copy.copy(self.vertices[t['v1']]),copy.copy(t))
                    intervals_0.append(copy.copy(interv))
                    #print(intervals_0[-1])
                    break
                elif v1 in trianglevertices2 and v2 in trianglevertices2:
                    #print(f"{len(bv1)} {len(boundary_vertices)} {i} {i+1} {bv1[i]} {bv1[i+1]} {len(self.vertices)} {t['v0']}")
                    #interv = BoundaryInterval(copy.copy(bv1[i]),copy.copy(bv1[i+1]),copy.copy(self.vertices[t['v0']]),copy.copy(t))
                    interv = BoundaryInterval(copy.copy(bv1[i]),copy.copy(bv1[i+1]),copy.copy(self.vertices[t['v0']]),copy.copy(t))
                    intervals_0.append(copy.copy(interv))
                    #print(intervals_0[-1])
                    break
        return intervals_0

    def distribute_points_to_intervals(self, points, intervals, symmetry_operations):
        """Distribute vertices to their corresponding intervals.

        Arguments:
        points -- list of vertices
        intervals -- list of intervals on the x axis
        symmetry_operations -- list of SymmetryOperations that need to be applied
        successively to map the vertices to the x axis

        Return: True if vertices were added to self.vertices, False else"""
        new_points = []
        for v in points:
            for bi in intervals:
                if bi.is_in_interval(v):
                    new_points.append(copy.copy(v))
                    if len(new_points) > 1:
                        new_points[-1]['index'] = new_points[-2]['index']+1
                    else:
                        new_points[-1]['index'] = len(self.vertices)
                    bi.add_interior_vertex(copy.copy(new_points[-1]))
        #Check if vertices already exist.
        #Assumption:    -If one vertex already exists, they all already exist.
        #               -If one vertex doesn't already exist, none does.
        #Could add debugging check if this is fulfilled, but expensive...
        if len(new_points) == 0:
            return False #no vertices to add
        want_to_add = True
        for op in symmetry_operations:
            self.apply_symmetry_operation(op)
        for v in self.vertices:
            #if(is_close(v['x'],new_points[0]['x']) and is_close(v['y'],new_points[0]['y'])):
            #if((abs(v['x']-new_points[0]['x']) < ISCLOSE_ABS_TOL) and (abs(v['y']-new_points[0]['y']) < ISCLOSE_ABS_TOL)):
            if(math.isclose(v['x'],new_points[0]['x'],abs_tol=ISCLOSE_ABS_TOL) and math.isclose(v['y'],new_points[0]['y'],abs_tol=ISCLOSE_ABS_TOL)):
                want_to_add = False #vertices already exist, nothing to add
                break
        if want_to_add:
            #if we get here, we want to add all vertices in new_points
            for v in new_points:
                self.vertices.append(copy.copy(v))
        for op in reversed(symmetry_operations):
            self.apply_symmetry_operation(op.inverse())
        return want_to_add

    def remove_refined_elements(self, intervals):
        """Remove elements that have been refined.

        Arguments:
        intervals -- list of BoundaryIntervals which contain the refined element info

        Notes:
        Any BoundaryInterval that has a point mapped to itself will trigger refinement
        of its corresponding element.
        """
        overcounty_elements = []
        for bi in intervals:
            if bi.element_needs_removal():
                overcounty_elements.append(bi.t['index'])
        #remove these elements
        self.triangles = [t for t in self.triangles if not(t['index'] in overcounty_elements)]

    def generate_new_elements(self, intervals):
        """Generate refined elements and add them to the mesh.

        Arguments:
        intervals -- list of BoundaryIntervals

        Notes:
        A BoundaryInterval can generate its refined elements."""
        for bi in intervals:
            if bi.element_needs_removal():
                new_elements = bi.get_elements()
                for t in new_elements:
                    self.triangles.append(copy.copy(t))

    def reindex_elements(self):
        """Fix element indexing.

        Notes:
        Some operations break the successive indexing of elements. This will make
        mshr fail. This function ensures correct element indices by assigning
        index i to element i.
        """
        for i in range(len(self.triangles)):
            self.triangles[i]['index'] = i

    def match_boundary_vertices(self, boundary_1_to_xaxis_ops, boundary_2_to_xaxis_ops):
        """Match vertices of two boundaries.

        Arguments:
        boundary_1_to_xaxis_ops -- list of SymmetryOperations that map boundary 1 to x axis
        boundary_2_to_xaxis_ops -- list of SymmetryOperations that map boundary 2 to x axis

        Notes:
        Matching works by first applying norm conserving functions """
        #copy mesh
        tmpmesh = copy.deepcopy(self)
        #map boundary 1 to x-axis
        for op in boundary_1_to_xaxis_ops:
            tmpmesh.apply_symmetry_operation(op)
        #extract boundary 1 vertices
        bv1 = tmpmesh.extract_xaxis_vertices()
        #extract boundary 1 intervals
        intervals_1 = tmpmesh.extract_boundary_intervals(bv1)
        #map boundary 2 to x-axis
        tmpmesh = copy.deepcopy(self)
        for op in boundary_2_to_xaxis_ops:
            tmpmesh.apply_symmetry_operation(op)

        #extract boundary 2 vertices
        bv2 = tmpmesh.extract_xaxis_vertices()
        #extract boundary 2 intervals
        intervals_2 = tmpmesh.extract_boundary_intervals(bv2)
        #distribute points to intervals and add new vertices to mesh vertices
        distributed_1 = self.distribute_points_to_intervals(bv2, intervals_1, boundary_1_to_xaxis_ops)
        distributed_2 = self.distribute_points_to_intervals(bv1, intervals_2, boundary_2_to_xaxis_ops)
        #remove refined elements
        if distributed_1:
            self.remove_refined_elements(intervals_1)
        if distributed_2:
            self.remove_refined_elements(intervals_2)
        if distributed_1:
            self.generate_new_elements(intervals_1)
        if distributed_2:
            self.generate_new_elements(intervals_2)
        #self.remove_refined_elements(intervals_1+intervals_2)
        #generate and add new elements
        #self.generate_new_elements(intervals_1+intervals_2)
        #re-index elements
        self.reindex_elements()

    #OBSOLETE: replaced by the above function match_boundary_vertices
    def match_boundaries(self,angle):
        """LEGACY: replaced by match_boundary_vertices."""
        #assumes a point is on the boundary iff y==0 before or after a CW rotation of angle
        #this can be relaxed with some more work
        intervals_0 = [] #lower boundary intervals
        intervals_1 = [] #upper boundary intervals
        #extract lower boundary vertices and corresponding elements
        bv1 = []
        for v in self.vertices:
            #if is_close(v['y'],0.):
            #if (abs(v['y']) < ISCLOSE_ABS_TOL):
            if math.isclose(v['y'],0.,abs_tol=ISCLOSE_ABS_TOL):
                bv1.append(copy.copy(v))
        bv1.sort(key=lambda v: v['x'])
        #extract corresponding elements and generate the BoundaryIntervals
        for i in range(len(bv1)-1):
            v1 = bv1[i]['index']
            v2 = bv1[i+1]['index']
            for t in self.triangles:
                trianglevertices0 = [t['v0'],t['v1']]
                trianglevertices1 = [t['v0'],t['v2']]
                trianglevertices2 = [t['v2'],t['v1']]
                if v1 in trianglevertices0 and v2 in trianglevertices0:
                    interv = BoundaryInterval(copy.copy(bv1[i]),copy.copy(bv1[i+1]),copy.copy(self.vertices[t['v2']]),copy.copy(t))
                    intervals_0.append(copy.copy(interv))
                    #print(f"v0: ({bv1[i]['x']}, {bv1[i]['y']})")
                    #print(f"v1: ({bv1[i+1]['x']}, {bv1[i+1]['y']})")
                    #print(intervals_0[-1])
                    break
                elif v1 in trianglevertices1 and v2 in trianglevertices1:
                    interv = BoundaryInterval(copy.copy(bv1[i]),copy.copy(bv1[i+1]),copy.copy(self.vertices[t['v1']]),copy.copy(t))
                    intervals_0.append(copy.copy(interv))
                    #print(f"v0: ({bv1[i]['x']}, {bv1[i]['y']})")
                    #print(f"v1: ({bv1[i+1]['x']}, {bv1[i+1]['y']})")
                    #print(intervals_0[-1])
                    break
                elif v1 in trianglevertices2 and v2 in trianglevertices2:
                    interv = BoundaryInterval(copy.copy(bv1[i]),copy.copy(bv1[i+1]),copy.copy(self.vertices[t['v0']]),copy.copy(t))
                    intervals_0.append(copy.copy(interv))
                    #print(f"v0: ({bv1[i]['x']}, {bv1[i]['y']})")
                    #print(f"v1: ({bv1[i+1]['x']}, {bv1[i+1]['y']})")
                    #print(intervals_0[-1])
                    break
        #same for upper boundary vertices
        self.rotate_mesh(-angle)
        bv2 = []
        for v in self.vertices:
            #if is_close(v['y'],0.):
            #if (abs(v['y']) < ISCLOSE_ABS_TOL):
            if math.isclose(v['y'],0.,abs_tol=ISCLOSE_ABS_TOL):
                bv2.append(copy.copy(v))
        bv2.sort(key=lambda v: v['x'])
        for i in range(len(bv2)-1):
            v1 = bv2[i]['index']
            v2 = bv2[i+1]['index']
            for t in self.triangles:
                trianglevertices0 = [t['v0'],t['v1']]
                trianglevertices1 = [t['v0'],t['v2']]
                trianglevertices2 = [t['v1'],t['v2']]
                if v1 in trianglevertices0 and v2 in trianglevertices0:
                    intervals_1.append(BoundaryInterval(copy.copy(bv2[i]),copy.copy(bv2[i+1]),copy.copy(self.vertices[t['v2']]),copy.copy(t)))
                    #print(intervals_1[-1])
                    break
                if v1 in trianglevertices1 and v2 in trianglevertices1:
                    intervals_1.append(BoundaryInterval(copy.copy(bv2[i]),copy.copy(bv2[i+1]),copy.copy(self.vertices[t['v1']]),copy.copy(t)))
                    #print(intervals_1[-1])
                    break
                if v1 in trianglevertices2 and v2 in trianglevertices2:
                    intervals_1.append(BoundaryInterval(copy.copy(bv2[i]),copy.copy(bv2[i+1]),copy.copy(self.vertices[t['v0']]),copy.copy(t)))
                    #print(intervals_1[-1])
                    break
        self.rotate_mesh(angle)
        #map all vertices from the other boundary into the correct boundary interval
        #lower boundary first
        #print(f"bv1 length: {len(bv1)}")
        #for v in bv1:
            #print(f"({v['x']}, {v['y']})")
        #print(f"bv2 length: {len(bv2)}")
        #for v in bv2:
            #print(f"({v['x']}, {v['y']})")
        #print(f"intervals_0 length: {len(intervals_0)}")
        #print(f"intervals_1 length: {len(intervals_1)}")
        #num_added = 0
        #print("From v2:")
        for v in bv2:
            for bi in intervals_0:
                if bi.is_in_interval(v):
                    self.vertices.append(copy.copy(v))
                    self.vertices[-1]['index'] = len(self.vertices)
                    bi.add_interior_vertex(copy.copy(self.vertices[-1]))
                    #num_added += 1
        #upper boundary next
        #print("From v1:")
        for v in bv1:
            for bi in intervals_1:
                if bi.is_in_interval(v):
                    self.vertices.append(copy.copy(v))
                    self.vertices[-1]['index'] = len(self.vertices)-1
                    bi.add_interior_vertex(copy.copy(self.vertices[-1]))
                    #now we need to rotate this vertex back to the upper boundary
                    rotated_coords = rotate([self.vertices[-1]['x'],self.vertices[-1]['y']],angle)
                    self.vertices[-1]['x'] = rotated_coords[0]
                    self.vertices[-1]['y'] = rotated_coords[1]
                    #num_added += 1
        #print(f"Added {num_added} vertices")
        #collect all elements that need to be removed
        overcounty_elements = []
        for bi in intervals_0+intervals_1:
            if bi.element_needs_removal():
                overcounty_elements.append(bi.t['index'])
        #remove these elements
        self.triangles = [t for t in self.triangles if not(t['index'] in overcounty_elements)]
        #generate and add new elements
        for bi in intervals_0+intervals_1:
            if bi.element_needs_removal():
                new_elements = bi.get_elements()
                for t in new_elements:
                    self.triangles.append(copy.copy(t))
        #reindex elements
        for i in range(len(self.triangles)):
            self.triangles[i]['index'] = i

        """
        #debugging
        self.to_file('tmpmsh.xml.gz')
        self.rotate_mesh(-60.*np.pi/180.)
        self.to_file('tmpmsh_rot.xml.gz')
        self.rotate_mesh(60.*np.pi/180.)
        tmpmsh = Mesh('tmpmsh.xml.gz')
        tmpmsh_rot = Mesh('tmpmsh_rot.xml.gz')
        print("Intervals_0:")
        for interv in intervals_0:
            plot(tmpmsh)
            plot(tmpmsh_rot)
            plt.plot([interv.v0['x'],interv.v1['x'],interv.vt['x'],interv.v0['x']],[interv.v0['y'],interv.v1['y'],interv.vt['y'],interv.v0['y']], c='r')
            plt.show()
        print("Intervals_1:")
        for interv in intervals_1:
            plot(tmpmsh)
            plot(tmpmsh_rot)
            plt.plot([interv.v0['x'],interv.v1['x'],interv.vt['x'],interv.v0['x']],[interv.v0['y'],interv.v1['y'],interv.vt['y'],interv.v0['y']],c='r')
            plt.show()
        """

    #TODO: This can be made more efficient, in place.
    #Copy individual vertices and act on them.
    def verify_symmetries(self, ops):
        """Check if the mesh is invariant under specific symmetry operations.

        Arguments:
        ops -- list of SymmetryOperations to check

        Return:
        list of booleans l, with l[i] True iff mesh is invariant under ops[i]
        """
        invariance = []
        for op in ops:
            meshcopy = copy.deepcopy(self)
            meshcopy.apply_symmetry_operation(op)
            for v in meshcopy.vertices:
                found_match = False
                for w in self.vertices:
                    #if is_close(v['x'],w['x']) and is_close(v['y'],w['y']):
                    #if (abs(v['x']-w['x']) < ISCLOSE_ABS_TOL) and (abs(v['y']-w['y']) < ISCLOSE_ABS_TOL):
                    if math.isclose(v['x'],w['x'],abs_tol=ISCLOSE_ABS_TOL) and math.isclose(v['y'],w['y'],abs_tol=ISCLOSE_ABS_TOL):
                        found_match = True
                        break
                if not found_match:
                    break
            invariance.append(found_match)
        return invariance

    #TODO: This can be made more efficient, in place.
    #Act on the vertices individually, then find their image.
    def get_matrix(self, op):
        """Get mesh transformation matrix for a symmetry operation.

        Arguments:
        op -- SymmetryOperation that maps the mesh to itself

        Return:
        NxN scipy.sparse.dok_matrix, with N the number of vertices present in the mesh

        Notes:
        Will raise a RuntimeError if the mesh is not invariant under op
        """
        mat = dok_matrix((len(self.vertices), len(self.vertices)), dtype=np.float32)
        mesh_copy = copy.deepcopy(self)
        mesh_copy.apply_symmetry_operation(op)
        for (i,v) in enumerate(mesh_copy.vertices):
            for (j,w) in enumerate(self.vertices):
                #if is_close(v['x'],w['x']) and is_close(v['y'],w['y']):
                #if (abs(v['x']-w['x']) < ISCLOSE_ABS_TOL) and (abs(v['y']-w['y']) < ISCLOSE_ABS_TOL):
                if math.isclose(v['x'],w['x'],abs_tol=ISCLOSE_ABS_TOL) and math.isclose(v['y'],w['y'],abs_tol=ISCLOSE_ABS_TOL):
                    mat[j,i] = 1.
                    break
                if j == len(self.vertices)-1:
                    raise RuntimeError("PMesh.get_matrix: Mesh is not invariant.")
        return mat

    def plot(self, linewidth=0.3):
        """Plot the mesh.

        Notes: Call plt.show() afterwards to view the plot
        """
        plt.axes().set_aspect('equal')
        for t in self.triangles:
            plt.plot([self.vertices[t['v0']]['x'],self.vertices[t['v1']]['x'],self.vertices[t['v2']]['x'],self.vertices[t['v0']]['x']],[self.vertices[t['v0']]['y'],self.vertices[t['v1']]['y'],self.vertices[t['v2']]['y'],self.vertices[t['v0']]['y']], lw=linewidth)

    def check_sanity(self):
        """Sanity check the mesh.

        Return:
        list [b1,b2,b3] of booleans.
        b1 -- is there a duplicate vertex?
        b2 -- is there an unconnected vertex?
        b3 -- is there a duplicate element?
        """
        vertex_duplicate = False
        vertex_unconnected = False
        element_duplicate = False
        #find duplicate vertices
        for i,v in enumerate(self.vertices):
            #l = [i for i,w in enumerate(self.vertices) if is_close(w['x'],v['x']) and is_close(w['y'],v['y'])]
            #l = [i for i,w in enumerate(self.vertices) if (abs(v['x']-w['x']) < ISCLOSE_ABS_TOL) and (abs(v['y']-w['y']) < ISCLOSE_ABS_TOL)]
            #l = [i for i,w in enumerate(self.vertices) if math.isclose(w['x'],v['x'],abs_tol=ISCLOSE_ABS_TOL) and math.isclose(w['y'],v['y'],abs_tol=ISCLOSE_ABS_TOL)]
            #if len(l) != 1:
            #    vertex_duplicate = True
            #    break
            if vertex_duplicate:
                break
            for j in range(i+1,len(self.vertices)):
                w = self.vertices[j]
                if math.isclose(w['x'],v['x'],abs_tol=ISCLOSE_ABS_TOL) and math.isclose(w['y'],v['y'],abs_tol=ISCLOSE_ABS_TOL):
                    vertex_duplicate = True
                    #print(f"Duplicate at ({w['x']}, {w['y']})")
                    break
        #find unconnected vertices with high probability
        connected_indices = {}
        for t in self.triangles:
            connected_indices[t['v0']] = True
            connected_indices[t['v1']] = True
            connected_indices[t['v2']] = True
        if len(connected_indices.keys()) != len(self.vertices):
            vertex_unconnected = True
        #for v in self.vertices:
        #    index = v['index']
        #    for i,t in enumerate(self.triangles):
        #        if index in [t['v0'],t['v1'],t['v2']]:
        #            break
        #        elif i == len(self.triangles)-1:
        #            vertex_unconnected = True
        #    if vertex_unconnected == True:
        #        break
        #find duplicate elements
        for i,t in enumerate(self.triangles):
            il = [t['v0'],t['v1'],t['v2']]
            for j in range(i+1,len(self.triangles)):
                r = self.triangles[j]
                if r['v0'] in il and r['v1'] in il and r['v2'] in il:
                    element_duplicate = True
                    break
            if element_duplicate:
                break
            #l = [i for i,r in enumerate(self.triangles) if r['v0'] in il and r['v1'] in il and r['v2'] in il]
            #if len(l) != 1:
            #    element_duplicate = True
            #    break
        return [vertex_duplicate, vertex_unconnected, element_duplicate]

    def __str__(self):
        """Convert mesh into a mshr compatible string."""
        printstring = ""
        #preamble bullshit
        printstring += "<?xml version=\"1.0\"?>\n"
        printstring += "<dolfin xmlns:dolfin=\"http://fenicsproject.org\">\n"
        #mesh type
        printstring += f"  <mesh celltype=\"{self.mesh_type}\" dim=\"{self.dimension}\">\n"
        #vertices
        printstring += f"    <vertices size=\"{len(self.vertices)}\">\n"
        for v in self.vertices:
            printstring += "      <vertex index=\"{0}\" x=\"{1:19.15e}\" y=\"{2:19.15e}\" />\n".format(v['index'], v['x'], v['y'])
        printstring += "    </vertices>\n"
        #cells
        printstring += f"    <cells size=\"{len(self.triangles)}\">\n"
        for t in self.triangles:
            printstring += f"      <triangle index=\"{t['index']}\" v0=\"{t['v0']}\" v1=\"{t['v1']}\" v2=\"{t['v2']}\" />\n"
        printstring += "    </cells>\n"
        #postamble bullshit
        printstring += "    <data />\n"
        printstring += "  </mesh>\n"
        printstring += "</dolfin>"

        return printstring

    def to_file(self, filename):
        """Save mesh to gzipped file.

        Arguments:
        filename -- name of the file to write to, recommended to end in .xml.gz

        Notes:
        This functionality allows transfering meshes to mshr. The workflow consists
        of storing the mesh to a .xml.gz file, and then constructing a mshr Mesh
        from that file."""
        with gzip.open(filename, 'wb') as f:
            f.write(self.__str__().encode())

    def to_mshr_Mesh(self):
        """Convert to mshr::Mesh.

        Return:
        mshr::Mesh
        """
        self.to_file('meshes/tmp_conversion.xml.gz')
        retMesh = Mesh('meshes/tmp_conversion.xml.gz')
        return retMesh

    def from_mshr_Mesh(self, mesh):
        """Load mshr::Mesh data.

        Arguments:
        mesh -- mshr::Mesh to load
        """
        File('meshes/tmp_conversion.xml.gz') << mesh
        elements = load_mesh('meshes/tmp_conversion.xml.gz')
        self.load_elements(elements)

    def print_cpp_mesh(self, file):
        """Write mesh to a file in a format accessible to the path tracer.

        Argument:
        file -- filename to write to
        """
        printstring = f"{len(self.vertices)}  {len(self.triangles)}\n"
        for v in self.vertices:
            printstring += f"{v['index']}  {v['x']}  {v['y']}\n"
        for t in self.triangles:
            printstring += f"{t['index']}  {t['v0']}  {t['v1']}  {t['v2']}\n"

        with open(file, 'w') as f:
            f.write(printstring)
