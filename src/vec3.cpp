#include <vec3.hpp>
#include <raytracing.hpp>
#include <cassert>

vec3::vec3(): e{0., 0., 0.} {}

vec3::vec3(double x, double y, double z): e{x, y, z} {}

double vec3::x() const{
  return e[0];
}

double vec3::y() const{
  return e[1];
}

double vec3::z() const{
  return e[2];
}

vec3 vec3::operator-() const{
  return vec3(-e[0], -e[1], -e[2]);
}

double vec3::operator[](int i) const{
  return e[i];
}

double& vec3::operator[](int i){
  return e[i];
}

vec3& vec3::operator+=(const vec3& v){
  e[0] += v.e[0];
  e[1] += v.e[1];
  e[2] += v.e[2];
  return *this;
}

vec3& vec3::operator-=(const vec3& v){
  return (*this += (-v));
}

vec3& vec3::operator*=(const double t){
  e[0] *= t;
  e[1] *= t;
  e[2] *= t;
  return *this;
}

vec3& vec3::operator/=(const double t){
  return (*this *= (1./t));
}

double vec3::length() const{
  return std::sqrt(length_squared());
}

double vec3::length_squared() const{
  return e[0]*e[0] + e[1]*e[1] + e[2]*e[2];
}

bool vec3::near_zero() const{
  const auto s = 1e-8;
  return (std::fabs(e[0]) < s) && (std::fabs(e[1]) < s) && (std::fabs(e[2]) < s);
}

vec3 vec3::random(){
  return vec3(random_double(), random_double(), random_double());
}

vec3 vec3::random(double min, double max){
  return vec3(random_double(min, max), random_double(min, max), random_double(min, max));
}

/*Utility*/
std::ostream& operator<<(std::ostream & out, const vec3& v){
  return out << v[0] << " " << v[1] << " " << v[2];
}

vec3 operator+(vec3 lhs, const vec3& rhs){
  return lhs += rhs;
}

vec3 operator-(vec3 lhs, const vec3& rhs){
  return lhs -= rhs;
}

vec3 operator*(vec3 lhs, const vec3& rhs){
  lhs[0] *= rhs[0];
  lhs[1] *= rhs[1];
  lhs[2] *= rhs[2];
  return lhs;
}

vec3 operator*(vec3 lhs, const double t){
  return lhs *= t;
}

vec3 operator*(const double t, vec3 rhs){
  return rhs *= t;
}

vec3 operator/(vec3 v, double t){
  return v /= t;
}

double dot(const vec3& lhs, const vec3& rhs){
  return lhs[0]*rhs[0] + lhs[1]*rhs[1] + lhs[2]*rhs[2];
}

vec3 cross(const vec3& lhs, const vec3& rhs){
  return vec3(lhs[1]*rhs[2] - lhs[2]*rhs[1], lhs[2]*rhs[0] - lhs[0]*rhs[2], lhs[0]*rhs[1] - lhs[1]*rhs[0]);
}

vec3 unit_vector(vec3 v){
  return v / v.length();
}

vec3 random_in_unit_sphere(){
  while(true){
    auto p = vec3::random(-1.,1.);
    if(p.length_squared() >= 1.){
      continue;
    }
    else{
      return p;
    }
  }
}

vec3 reflect(const vec3& v, const vec3& n){
  assert(std::fabs(n.length() - 1.) < 0.00000001);
  return v - 2. * dot(v,n) * n;
}

vec3 refract(const vec3& unit_v, const vec3& n, double refraction_ratio){
  assert(std::fabs(unit_v.length() - 1.) < 0.00000001);
  assert(std::fabs(n.length() - 1.) < 0.00000001);
  auto cos_theta = fmin(dot(-unit_v, n), 1.);
  vec3 r_out_perp = refraction_ratio * (unit_v + cos_theta * n);
  vec3 r_out_parallel = -std::sqrt(std::fabs(1.0 - r_out_perp.length_squared())) * n;
  return r_out_perp + r_out_parallel;
}
