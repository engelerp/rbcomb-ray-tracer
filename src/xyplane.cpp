#include <xyplane.hpp>

xyplane::xyplane(): z(0){

}

xyplane::xyplane(double z, std::shared_ptr<material> mat_ptr)
: z(z), normal(0.,0.,1.), mat_ptr(mat_ptr) {

}

bool xyplane::hit(
const ray& r, double t_min, double t_max, hit_record& rec) const{

  if(r.direction().z() == 0.){
    return false;
  }
  double t_intersect = (z - r.origin().z()) / r.direction().z();
  if(t_intersect <= t_min || t_intersect >= t_max){
    return false;
  }
  else{
    rec.t = t_intersect;
    rec.normal = normal;
    rec.p = r.at(t_intersect);
    rec.mat_ptr = mat_ptr;
    rec.inbound = true;
    return true;
  }
}
