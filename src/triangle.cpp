#include <triangle.hpp>
#include <vec3.hpp>
#include <cmath>
#include <iostream>

triangle::triangle(){

}

triangle::triangle(point3 c1, point3 c2, point3 c3, std::shared_ptr<material> m)
: c1(c1), c2(c2), c3(c3), mat_ptr(m){
  c1c2 = c2 - c1;
  c1c3 = c3 - c1;
  normal = unit_vector(cross(c1c2, c1c3));
  D = - dot(normal, c1);
  #ifndef NDEBUG
  std::cerr << "Triangle normal: " << normal << ", D: " << D << std::endl;
  #endif
}

bool triangle::hit(const ray& r, double t_min, double t_max, hit_record& rec) const{
  /*step 1: intersect with plane*/
  /*if ray direction is parallel to plane, no intersection is possible*/
  double dirdotnormal = dot(normal, r.direction());
  if(std::fabs(dirdotnormal) < 0.00000001) {
    #ifndef NDEBUG
    std::cerr << "Ray parallel to triangle" << std::endl;
    #endif
    return false;
  }
  /*find intersecting t*/
  double t_intersect = - (D + dot(normal, r.origin())) / dirdotnormal;
  if(t_intersect <= t_min || t_intersect >= t_max){
    #ifndef NDEBUG
    std::cerr << "Intersection outside range: t=" << t_intersect << ", p=" << r.at(t_intersect) << std::endl;
    #endif
    return false;
  }

  #ifndef NDEBUG
  std::cerr << "Triangle intersected at t=" << t_intersect << ", p=" << r.at(t_intersect) << std::endl;
  std::cerr << "Ray origin: " << r.origin() << ", direction: " << r.direction() << std::endl;
  std::cerr << "Ray bounces left: " << r.get_bounces_left() << std::endl;
  #endif

  point3 Spt = r.at(t_intersect);
  vec3 c1s = Spt - c1;
  /*check if intersection is within triangle*/
  /*change basis, c1s = a * c1c3 + b * c1c2 */
  /*no orthogonal basis means we need to solve a system of equations*/
  /*this only works if c1c3 and c1c2 are not collinear*/
  auto alpha = dot(c1s, c1c3);
  auto beta = c1c3.length_squared();
  auto gamma = dot(c1c3, c1c2);
  auto delta = dot(c1s, c1c2);
  auto epsilon = c1c2.length_squared();
  auto denominator = gamma * gamma - beta * epsilon;
  auto a = (delta * gamma - alpha * epsilon) / denominator;
  auto b = (alpha * gamma - beta * delta) / denominator;

  /*fill in the hit record and return true if we intersect within triangle*/
  if((a + b <= 1.) && (a >= 0.) && (b >= 0.)){
    #ifndef NDEBUG
    std::cerr << "Intersection within triangle" << std::endl;
    #endif
    rec.p = Spt;
    rec.normal = normal;
    rec.t = t_intersect;
    rec.mat_ptr = mat_ptr;
    rec.inbound = (dot(normal, r.direction()) < 0);
    return true;
  }
  /*else we return false*/
  else{
    #ifndef NDEBUG
    std::cerr << "Intersection outside triangle" << std::endl;
    #endif
    return false;
  }
}
