#include <wavelength2rgb.hpp>

wavelength2rgb::wavelength2rgb(double nm_min, double nm_max, double nm_step)
: nm_min(nm_min), nm_max(nm_max), nm_step(nm_step){
  //TODO: precompute the values
}

/*
  Inspired by Urs Hofmann (hofmannu@biomed.ee.ethz.ch)
  https://github.com/razanskylab/wavelength2rgb/blob/master/wavelength2color.m
*/
color wavelength2rgb::operator()(double nm) const{
  color colvec;

  if(nm >= 380. && nm < 440.){
    colvec[0] = -(nm - 440.) / (440. - 380.);
    colvec[1] = 0.;
    colvec[2] = 1.;
    if(nm < 420.){
      double factor = 0.3 + 0.7 * (nm - 380.) / (420. - 380.);
      colvec *= factor;
    }
  }
  else if(nm >= 440. && nm < 490.){
    colvec[0] = 0.;
    colvec[1] = (nm - 440.) / (490. - 440.);
    colvec[2] = 1.;
  }
  else if(nm >= 490. && nm < 510.){
    colvec[0] = 0.;
    colvec[1] = 1.;
    colvec[2] = -(nm - 510.) / (510. - 490.);
  }
  else if(nm >= 510. && nm < 580.){
    colvec[0] = (nm - 510.) / (580. - 510.);
    colvec[1] = 1.;
    colvec[2] = 0.;
  }
  else if(nm >= 580. && nm < 645.){
    colvec[0] = 1.;
    colvec[1] = -(nm - 645.) / (645. - 580.);
    colvec[2] = 0.;
  }
  else if(nm >= 645. && nm < 780.){
    colvec[0] = 1.;
    colvec[1] = 0.;
    colvec[2] = 0.;
    if(nm > 700.){
      double factor = 0.3 + 0.7 * (780. - nm) / (780. - 700.);
      colvec *= factor;
    }
  }
  else{
    colvec[0] = 0.;
    colvec[1] = 0.;
    colvec[2] = 0.;
  }
  return colvec;
}
