#include <dielectric.hpp>
#include <cmath>
#include <cassert>

dielectric::dielectric(double index_of_refraction_inside, double index_of_refraction_outside)
: ir_inside(index_of_refraction_inside), ir_outside(index_of_refraction_outside){

}

bool dielectric::scatter(
  const ray& r_in, const hit_record& rec,
  double& fraction_reflected, ray& r_reflected,
  double& fraction_refracted, ray& r_refracted
) const{
  int bounces_left = r_in.get_bounces_left();
  if(bounces_left == 0){
    //ray dies here
    fraction_reflected = 0.;
    fraction_refracted = 0.;
    return false;
  }
  double optical_path_to_p = r_in.optical_path_to(rec.t);
  vec3 unit_direction = unit_vector(r_in.direction());
  double refraction_ratio;
  double ir_in, ir_out;
  vec3 relevant_normal;
  if(rec.inbound){
    refraction_ratio = ir_outside / ir_inside;
    ir_in = ir_outside;
    ir_out = ir_inside;
    relevant_normal = rec.normal;
  }
  else{
    refraction_ratio = ir_inside / ir_outside;
    ir_in = ir_inside;
    ir_out = ir_outside;
    relevant_normal = -rec.normal;
  }

  double cos_theta = std::fmin(dot(-unit_direction, relevant_normal), 1.);
  double sin_theta = std::sqrt(1. - cos_theta * cos_theta);

  //total reflection?
  bool total_reflection = (refraction_ratio * sin_theta > 1.);

  //reflect
  vec3 dir_reflected = reflect(unit_direction, relevant_normal);
  //refract if we don't have total reflection
  vec3 dir_refracted;
  if(!total_reflection){
    dir_refracted = refract(unit_direction, relevant_normal, refraction_ratio);
    double sin_theta_refracted = refraction_ratio * sin_theta;
    double cos_theta_refracted = std::sqrt(1. - sin_theta_refracted * sin_theta_refracted);
    fraction_reflected = reflectance(cos_theta, cos_theta_refracted, ir_in, ir_out);
    //fraction_refracted = 1. - fraction_reflected;
    fraction_refracted = transmittance(cos_theta, cos_theta_refracted, ir_in, ir_out);
  }
  else{
    fraction_reflected = 1.;
    fraction_refracted = 0.;
  }
  int phase_flips = r_in.get_phaseflips();
  /*
  //Explicit handling of phase flips
  r_reflected = ir_out > ir_in ?
                ray(rec.p, dir_reflected, fraction_reflected * r_in.get_amplitude_at(rec.t), optical_path_to_p, ir_in, extinct_in, phase_flips + 1, bounces_left - 1) :
                ray(rec.p, dir_reflected, fraction_reflected * r_in.get_amplitude_at(rec.t), optical_path_to_p, ir_in, extinct_in, phase_flips, bounces_left - 1);
  */
  //Phase flips are absorbed by the reflection amplitude
  r_reflected = ray(rec.p, dir_reflected, fraction_reflected * r_in.get_amplitude(), optical_path_to_p, ir_in, phase_flips, bounces_left - 1);
  r_refracted = ray(rec.p, dir_refracted, fraction_refracted * r_in.get_amplitude(), optical_path_to_p, ir_out, phase_flips, bounces_left - 1);

  #ifndef NDEBUG
  //if(fraction_reflected > 1.){
    std::cerr << "Reflected: " << fraction_reflected << ", Refracted: " << fraction_refracted << std::endl;
    std::cerr << "cos_theta: " << cos_theta << std::endl;
    std::cerr << "sin_theta: " << sin_theta << std::endl;
    std::cerr << "normal: " << rec.normal << std::endl;
    std::cerr << "reflected ray: origin: " << r_reflected.origin() << ", direction: " << r_reflected.direction() << std::endl;
    std::cerr << "refracted ray: origin: " << r_refracted.origin() << ", direction: " << r_refracted.direction() << std::endl;
  //}
  #endif
  //assert(fraction_reflected >= 0. && fraction_refracted >= 0.);
  //assert(fraction_reflected <= 1. && fraction_refracted <= 1.);
  //assert(std::fabs(fraction_refracted + fraction_reflected - 1.) < 0.00000001);
  assert(r_reflected.origin().x() == rec.p.x() && r_reflected.origin().y() == rec.p.y() && r_reflected.origin().z() == rec.p.z());
  assert(r_refracted.origin().x() == rec.p.x() && r_refracted.origin().y() == rec.p.y() && r_refracted.origin().z() == rec.p.z());

  return true;
}

double dielectric::reflectance(double cos_theta_in, double cos_theta_out, double ir_in, double ir_out) const{
  /*
  //old "power" reflectance coefficients
  double Rs = std::pow((ir_in*cos_theta_in - ir_out*cos_theta_out)/(ir_in*cos_theta_in + ir_out*cos_theta_out),2.);
  double Rp = std::pow((ir_in*cos_theta_out - ir_out*cos_theta_in)/(ir_in*cos_theta_out + ir_out*cos_theta_in),2.);
  */
  //new amplitude reflectance coefficients
  double rs = ( ir_in * cos_theta_in - ir_out * cos_theta_out ) / ( ir_in * cos_theta_in + ir_out * cos_theta_out );
  //double rp = ( ir_out * cos_theta_in - ir_in * cos_theta_out ) / ( ir_out * cos_theta_in + ir_in * cos_theta_out );
  return rs;
}

double dielectric::transmittance(double cos_theta_in, double cos_theta_out, double ir_in, double ir_out) const{
  double ts = 2 * ir_in * cos_theta_in / ( ir_in * cos_theta_in + ir_out * cos_theta_out );
  //double tp = 2 * ir_in * cos_theta_in / ( ir_out * cos_theta_in + ir_in * cos_theta_out );
  return ts;
}
