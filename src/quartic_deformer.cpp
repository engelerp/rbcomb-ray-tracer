#include <quartic_deformer.hpp>
#include <vec3.hpp>

quartic_deformer::quartic_deformer(double maxelong, double z_zero, double up, point3 center)
: maxelong(maxelong), z_zero(z_zero), up(up), center(center){

}

double quartic_deformer::z(double x, double y) const{
  double r_sq = (center.x()-x)*(center.x()-x) + (center.y()-y)*(center.y()-y);
  return z_zero + up * maxelong * (16 * r_sq * r_sq - 1);
}
