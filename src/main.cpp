#include <raytracing.hpp>
#include <camera.hpp>
#include <hittable.hpp>
#include <triangle.hpp>
#include <xyplane.hpp>
#include <hittable_list.hpp>
#include <triangle_mesh.hpp>
#include <material.hpp>
#include <dielectric.hpp>
#include <radiator.hpp>
#include <iostream>
#include <memory>
#include <quartic_deformer.hpp>
#include <quadratic_deformer.hpp>
#include <linear_deformer.hpp>
#include <interpolating_deformer.hpp>
#include <constant_deformer.hpp>
#include <wavelength2rgb.hpp>
#include <chrono>
#ifdef _OPENMP
#include <omp.h>
#endif

color pixel_color(const ray& r, const hittable& world, const std::vector<vec3> source_color, std::vector<ray>& rays_en_route, std::vector<ray>& rays_arrived, const std::vector<color>& colors, const std::vector<double>& wavelengths){
  rays_en_route.clear();
  rays_arrived.clear();
  ray r_curr;
  ray r_refl, r_refr;
  double frac_refl = 0., frac_refr = 0.;
  hit_record rec;

  rays_en_route.push_back(r);
  while(!rays_en_route.empty()){
    #ifndef NDEBUG
    char dummy;
    std::cin >> dummy;
    std::cerr << "rays en route: " << rays_en_route.size() << std::endl;
    std::cerr << "rays arrived: " << rays_arrived.size() << std::endl;
    #endif
    r_curr = rays_en_route.back();
    rays_en_route.pop_back();
    if(world.hit(r_curr, 0.0000001, infinity, rec)){
      /*ray hits something*/
      bool status = rec.mat_ptr->scatter(r_curr, rec, frac_refl, r_refl, frac_refr, r_refr);
      if(status){
        #ifndef NDEBUG
        std::cerr << "Reflected: " << frac_refl << ", Refracted: " << frac_refr << "\n";
        #endif
        /*We scattered*/
        if(std::fabs(r_refl.get_amplitude()) >= 1e-6){
          rays_en_route.push_back(r_refl);
        }
        if(std::fabs(r_refr.get_amplitude()) >= 1e-6){
          rays_en_route.push_back(r_refr);
        }
      }
      else{
        if(frac_refl != 0.){
          //ray arrived at source
          rays_arrived.push_back(r_refl);
        }
        //else we discard the ray (nothing to do here)
      }
    }
  }
  //all rays are done
  vec3 colour (0., 0., 0.);
  for(size_t i = 0; i < wavelengths.size(); ++i){
    double colour_amplitude = 0.;
    int num_phase_av = 0;
    //average over phase offset, as reality does
    for(double phase_offset = 0.; phase_offset < 2. * pi; phase_offset += 0.31){
      ++num_phase_av;
      double inner_amp = 0.;
      for(const auto& r: rays_arrived){
        if(r.get_source() == 1){ //backlight is turned off
          continue;
        }
        //rays interfere here
        inner_amp += r.get_amplitude() * std::cos(phase_offset + r.optical_path_to(0.) * 2. * pi / wavelengths[i]);
      }
      //colour amplitude is intensity
      colour_amplitude += inner_amp * inner_amp;
    }
    colour_amplitude /= static_cast<double>(num_phase_av);
    colour += colour_amplitude * colors[i];
  }
  /*
  for(const auto& r: rays_arrived){
    color += r.get_amplitude() * source_color[r.get_source()];
  }
  */
  return colour / static_cast<double>(colors.size());
}







int main(){
  auto start_main = std::chrono::high_resolution_clock::now();
  #ifdef _OPENMP
  std::cerr << "Available Processing Units: " << omp_get_num_procs() << std::endl;
  #endif

  /*Image*/
  const int image_width = 512;
  const int image_height = 512;

  /*World*/
  hittable_list world;

  double n_si3n4 = 2.;
  double n_sio2 = 1.45;
  double n_si = 1e8;
  double n_air = 1.;

  auto material_si3n4_air   = std::make_shared<dielectric>(n_si3n4, n_air);
  auto material_si3n4_sio2  = std::make_shared<dielectric>(n_si3n4, n_sio2);
  auto material_si_sio2     = std::make_shared<dielectric>(n_si, n_sio2);
  auto material_si_air     = std::make_shared<dielectric>(n_si, n_air);
  auto material_backplane   = std::make_shared<radiator>(1);
  auto material_frontplane  = std::make_shared<radiator>(2);

/*
  vec3 tc1(0.5, 0.5, -1.3);
  vec3 tc2(-0.5, 0.5, -1.3);
  vec3 tc3(-0.5, -0.5, -1.3);
  double delta = 0.05;
  for(int i = 0; i < 100; ++i){
    tc1[2] -= delta;
    tc2[2] -= delta;
    tc3[2] -= delta;
    world.add(std::make_shared<triangle>(tc1, tc2, tc3, material_triangle));
  }
*/
  /*Mesh deformers*/
  double extra_gap = 0.;
  /*Si3N4*/
  constant_deformer top_def_si_air(-1.6192);
  constant_deformer top_def_si_sio2(-1.9992);
  constant_deformer top_def_sio2_si3n4(-1.9995);
  constant_deformer top_def_const_1(-1.9995);
  constant_deformer top_def_const_2(-2.);
  constant_deformer bot_def_const_1(-2.001-extra_gap);
  constant_deformer bot_def_const_2(-2.0015-extra_gap);
  /*
  quartic_deformer  top_def_quart_1(0.0009, -1.9995, 1., vec3(0., 0., 0.));
  quartic_deformer  top_def_quart_2(0.0009, -2.0000, 1., vec3(0., 0., 0.));
  quartic_deformer  bot_def_quart_d0_1(0.000009, -2.001, -1., vec3(0., 0.76, 0.));
  quartic_deformer  bot_def_quart_d1_1(0.000009, -2.001, -1., vec3(-0.6581793068761733, -0.38, 0.));
  quartic_deformer  bot_def_quart_d2_1(0.000009, -2.001, -1., vec3(0.6581793068761733, -0.38, 0.));
  quartic_deformer  bot_def_quart_d0_2(0.000009, -2.0015, -1., vec3(0., 0.76, 0.));
  quartic_deformer  bot_def_quart_d1_2(0.000009, -2.0015, -1., vec3(-0.6581793068761733, -0.38, 0.));
  quartic_deformer  bot_def_quart_d2_2(0.000009, -2.0015, -1., vec3(0.6581793068761733, -0.38, 0.));
  */
  /*
  quadratic_deformer  top_def_quad_1(0.000999, -1.9995, 1., vec3(0., 0., 0.));
  quadratic_deformer  top_def_quad_2(0.000999, -2.0000, 1., vec3(0., 0., 0.));
  quadratic_deformer  bot_def_quad_d0_1(0.000999, -2.001, -1., vec3(0., 0.76, 0.));
  quadratic_deformer  bot_def_quad_d1_1(0.000999, -2.001, -1., vec3(-0.6581793068761733, -0.38, 0.));
  quadratic_deformer  bot_def_quad_d2_1(0.000999, -2.001, -1., vec3(0.6581793068761733, -0.38, 0.));
  quadratic_deformer  bot_def_quad_d0_2(0.000999, -2.0015, -1., vec3(0., 0.76, 0.));
  quadratic_deformer  bot_def_quad_d1_2(0.000999, -2.0015, -1., vec3(-0.6581793068761733, -0.38, 0.));
  quadratic_deformer  bot_def_quad_d2_2(0.000999, -2.0015, -1., vec3(0.6581793068761733, -0.38, 0.));
  */
  /*
  linear_deformer  top_def_quad_1(0.000999, -1.9995, 1., vec3(0., 0., 0.));
  linear_deformer  top_def_quad_2(0.000999, -2.0000, 1., vec3(0., 0., 0.));
  linear_deformer  bot_def_quad_d0_1(0., -2.001, -1., vec3(0., 0.76, 0.));
  linear_deformer  bot_def_quad_d1_1(0., -2.001, -1., vec3(-0.6581793068761733, -0.38, 0.));
  linear_deformer  bot_def_quad_d2_1(0., -2.001, -1., vec3(0.6581793068761733, -0.38, 0.));
  linear_deformer  bot_def_quad_d0_2(0., -2.0015, -1., vec3(0., 0.76, 0.));
  linear_deformer  bot_def_quad_d1_2(0., -2.0015, -1., vec3(-0.6581793068761733, -0.38, 0.));
  linear_deformer  bot_def_quad_d2_2(0., -2.0015, -1., vec3(0.6581793068761733, -0.38, 0.));
  */
  interpolating_deformer  top_def_quad_1("../resources/deformer_0.dat", 0.000999+extra_gap, -1.9995, 1., vec3(0., 0., 0.));
  interpolating_deformer  top_def_quad_2("../resources/deformer_0.dat", 0.000999+extra_gap, -2.0000, 1., vec3(0., 0., 0.));
  interpolating_deformer  bot_def_quad_d0_1("../resources/deformer_0.dat", 0., -2.001-extra_gap, -1., vec3(0., 0.76, 0.));
  interpolating_deformer  bot_def_quad_d1_1("../resources/deformer_0.dat", 0., -2.001-extra_gap, -1., vec3(-0.6581793068761733, -0.38, 0.));
  interpolating_deformer  bot_def_quad_d2_1("../resources/deformer_0.dat", 0., -2.001-extra_gap, -1., vec3(0.6581793068761733, -0.38, 0.));
  interpolating_deformer  bot_def_quad_d0_2("../resources/deformer_0.dat", 0., -2.0015-extra_gap, -1., vec3(0., 0.76, 0.));
  interpolating_deformer  bot_def_quad_d1_2("../resources/deformer_0.dat", 0., -2.0015-extra_gap, -1., vec3(-0.6581793068761733, -0.38, 0.));
  interpolating_deformer  bot_def_quad_d2_2("../resources/deformer_0.dat", 0., -2.0015-extra_gap, -1., vec3(0.6581793068761733, -0.38, 0.));
  constant_deformer bot_def_sio2_si3n4(-2.0015-extra_gap);
  constant_deformer bot_def_si_sio2(-2.0018-extra_gap);
  constant_deformer bot_def_si_air(-2.3818-extra_gap);

  /*Top Wafer*/
  //world.add(std::make_shared<triangle_mesh>("../meshes/mesh_top_tot.dat", material_si3n4, 1., &top_def_const_1));
  //Top layer
  world.add(std::make_shared<triangle_mesh>("../meshes/mesh_top_rect_adaptive2.dat", material_si_air, 1., &top_def_si_air));
  world.add(std::make_shared<triangle_mesh>("../meshes/mesh_top_rect_adaptive2.dat", material_si_sio2, -1., &top_def_si_sio2));
  world.add(std::make_shared<triangle_mesh>("../meshes/mesh_top_rect_adaptive2.dat", material_si3n4_sio2, 1., &top_def_sio2_si3n4));
  world.add(std::make_shared<triangle_mesh>("../meshes/mesh_top_drum_adaptive2.dat", material_si3n4_air, 1., &top_def_quad_1));
  //world.add(std::make_shared<triangle_mesh>("../meshes/mesh_top_rect.dat", material_si3n4_air, 1., &top_def_const_1));
  //Bottom layer
  world.add(std::make_shared<triangle_mesh>("../meshes/mesh_top_drum_adaptive2.dat", material_si3n4_air, -1., &top_def_quad_2));
  world.add(std::make_shared<triangle_mesh>("../meshes/mesh_top_rect_adaptive2.dat", material_si3n4_air, -1., &top_def_const_2));

  /*Bottom Wafer*/
  //Top layer
  world.add(std::make_shared<triangle_mesh>("../meshes/mesh_bottom_drum0.dat", material_si3n4_air, 1., &bot_def_quad_d0_1));
  world.add(std::make_shared<triangle_mesh>("../meshes/mesh_bottom_drum1.dat", material_si3n4_air, 1., &bot_def_quad_d1_1));
  world.add(std::make_shared<triangle_mesh>("../meshes/mesh_bottom_drum2.dat", material_si3n4_air, 1., &bot_def_quad_d2_1));
  world.add(std::make_shared<triangle_mesh>("../meshes/mesh_bottom_rect.dat", material_si3n4_air, 1., &bot_def_const_1));
  //Bottom layer
  world.add(std::make_shared<triangle_mesh>("../meshes/mesh_bottom_drum0.dat", material_si3n4_air, -1., &bot_def_quad_d0_2));
  world.add(std::make_shared<triangle_mesh>("../meshes/mesh_bottom_drum1.dat", material_si3n4_air, -1., &bot_def_quad_d1_2));
  world.add(std::make_shared<triangle_mesh>("../meshes/mesh_bottom_drum2.dat", material_si3n4_air, -1., &bot_def_quad_d2_2));
  world.add(std::make_shared<triangle_mesh>("../meshes/mesh_bottom_rect.dat", material_si3n4_sio2, -1., &bot_def_sio2_si3n4));
  world.add(std::make_shared<triangle_mesh>("../meshes/mesh_bottom_rect.dat", material_si_sio2, 1., &bot_def_si_sio2));
  world.add(std::make_shared<triangle_mesh>("../meshes/mesh_bottom_rect.dat", material_si_air, -1., &bot_def_si_air));


  world.add(std::make_shared<xyplane>(-10., material_backplane));
  world.add(std::make_shared<xyplane>(1., material_frontplane));

  /*Source colors*/
  std::vector<vec3> source_color;
  source_color.push_back(vec3(0.,0.,0.));
  source_color.push_back(vec3(0.1,0.,0.));
  source_color.push_back(vec3(0.,0.,1.));

  /*Colors and corresponding wavelengths*/
  //wavelengths emitted by lamps, in nm
  double lambda_start = 380.;
  double lambda_end = 780.;
  double lambda_step = 1.;
  std::vector<double> wavelengths; //wavelengths in mm
  std::vector<color> colors;
  wavelength2rgb nm2rgb(lambda_start, lambda_end, lambda_step);
  for(double lambda = lambda_start; lambda < lambda_end; ++lambda){
    wavelengths.push_back(lambda/1000000.);
    colors.push_back(nm2rgb(lambda));
  }

  /*Color Storage*/
  std::vector<color> color_storage (image_width * image_height);

  /*Camera*/
  camera cam;

  /*Render*/
  std::cout << "P3\n" << image_width << " " << image_height << "\n255\n";

  auto start_parallel = std::chrono::high_resolution_clock::now();
  #ifdef _OPENMP
  int scanlines_done = 0;
  #pragma omp parallel shared(cam, world, colors, wavelengths, color_storage, scanlines_done)
  {
  #endif
  std::vector<ray> rays_en_route;
  rays_en_route.reserve(1024);
  std::vector<ray> rays_arrived;
  rays_arrived.reserve(1024);
  #ifdef _OPENMP
  #pragma omp for schedule(runtime)
  #endif
  for(int j = image_height-1; j >= 0; --j){
    #ifndef _OPENMP
    std::cerr << "\rScanlines remaining: " << j << " " << std::flush;
    #endif
    for(int i = 0; i < image_width; ++i){
      double u = static_cast<double>(i) / (image_width - 1);
      double v = static_cast<double>(j) / (image_height - 1);
      ray r = cam.get_ray(u, v);
      vec3 pix_col = pixel_color(r, world, source_color, rays_en_route, rays_arrived, colors, wavelengths);
      #ifdef _OPENMP
      #pragma omp critical
      {
      #endif
      color_storage[(image_height-1-j)*image_height + i] = pix_col;
      #ifdef _OPENMP
      }
      #endif
      //write_color(std::cout, pix_col, 1);
    }
    #ifdef _OPENMP
    #pragma omp critical
    {
    scanlines_done = scanlines_done + 1;
    std::cerr << "\rScanlines done: " << scanlines_done << "/" << image_height << " " << std::flush;
    }
    #endif

  }
  #ifdef _OPENMP
  }
  #endif
  auto end_parallel = std::chrono::high_resolution_clock::now();
  for(const auto& col: color_storage){
    write_color(std::cout, col, 1);
  }
  std::cerr << "\nDone.\n";
  auto end_main = std::chrono::high_resolution_clock::now();
  using namespace std::literals;
  std::cerr << "\nTiming Information:\n"
            << "Setup Time: " << (start_parallel - start_main) / 1us / 1000000. << "s\n"
            << "Parallel Time: " << static_cast<int>((end_parallel - start_parallel) / 1h) << "h " << static_cast<int>((end_parallel - start_parallel) / 1min) % 60 << "min " << static_cast<int>((end_parallel - start_parallel) / 1s) % 60 << "s\n"
            << "Output Time: " << (end_main - end_parallel) / 1us / 1000000. << "s\n"
            << "Total Time: " << static_cast<int>((end_main - start_main) / 1h) << "h " << static_cast<int>((end_main - start_main) / 1min) % 60 << "min " << static_cast<int>((end_main - start_main) / 1s) % 60 << "s\n";
}
