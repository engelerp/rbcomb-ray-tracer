#include <constant_deformer.hpp>

constant_deformer::constant_deformer(double z): z_const(z){

}

double constant_deformer::z(double x, double y) const{
  return z_const;
}
