#include <ray.hpp>

ray::ray() {}

ray::ray(const point3& origin, const vec3& direction, double amplitude, double opl, double index_of_refraction, int phase_flips, int max_bounces)
: orig(origin), dir(direction), optical_path(opl), refractive_index(index_of_refraction), amplitude(amplitude),
  phase_flips(phase_flips), source(0), bounces_left(max_bounces){

}

point3 ray::origin() const{
  return orig;
}

vec3 ray::direction() const{
  return dir;
}

point3 ray::at(double t) const{
  return orig + t * dir;
}

double ray::optical_path_to(double t) const{
  return optical_path + (at(t) - orig).length() * refractive_index;
}
double ray::refractive_index_current() const{
  return refractive_index;
}

int ray::get_bounces_left() const{
  return bounces_left;
}

int ray::get_phaseflips() const{
  return phase_flips;
}

void ray::set_source(int n){
  source = n;
}

int ray::get_source() const{
  return source;
}

double ray::get_amplitude() const{
  return amplitude;
}
