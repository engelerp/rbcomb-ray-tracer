#include <triangle_mesh.hpp>
#include <fstream>
#include <iostream>

triangle_mesh::triangle_mesh(std::string filename, std::shared_ptr<material> m, double n_z_sign, const deformer* fun)
: mesh_file(filename)
{
  std::fstream file(mesh_file);
  size_t num_vertices, num_triangles;
  file >> num_vertices >> num_triangles;

  triangles = std::vector<triangle>(num_triangles);
  std::vector<point3> vertices (num_vertices);

  //Load vertices
  size_t index;
  double x, y, z;
  for(size_t i = 0; i < num_vertices; ++i){
    file >> index >> x >> y;
    z = fun->z(x, y);
    vertices[index] = point3(x, y, z);
  }
  //Load triangles
  size_t vi0, vi1, vi2;
  for(size_t i = 0; i < num_triangles; ++i){
    file >> index >> vi0 >> vi1 >> vi2;
    /*
      NOTE:
      We explicitly check the normal_z sign and fix it if necessary.
      This restricts meshes to surfaces facing only up or only down.
    */
    vec3 normal = unit_vector(cross(vertices[vi1]-vertices[vi0], vertices[vi2]-vertices[vi0]));
    if(normal.z()*n_z_sign > 0.){
      triangles[index] = triangle(vertices[vi0], vertices[vi1], vertices[vi2], m);
    }
    else{
      triangles[index] = triangle(vertices[vi0], vertices[vi2], vertices[vi1], m);
    }
  }
}

bool triangle_mesh::hit(
  const ray& r, double t_min, double t_max, hit_record& rec) const{
    hit_record temp_rec;
    bool hit_anything = false;
    double closest_so_far = t_max;

    /*Here we could probably break after the first hit*/
    for(const auto& triangle: triangles){
      if(triangle.hit(r, t_min, closest_so_far, temp_rec)){
        hit_anything = true;
        closest_so_far = temp_rec.t;
        rec = temp_rec;
      }
    }

    return hit_anything;
  }
