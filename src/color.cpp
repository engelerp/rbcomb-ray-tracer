#include <color.hpp>
#include <raytracing.hpp>
#include <cmath>

void write_color(std::ostream& out, const color& pixel_color, int samples_per_pixel){
  double scale = 1.0 / samples_per_pixel;

  /* Average with gamma 3.0 correction */
  double r = std::cbrt(pixel_color.x() * scale);
  double g = std::cbrt(pixel_color.y() * scale);
  double b = std::cbrt(pixel_color.z() * scale);

  out << static_cast<int>(256 * clamp(r, 0.0, 0.999)) << " "
      << static_cast<int>(256 * clamp(g, 0.0, 0.999)) << " "
      << static_cast<int>(256 * clamp(b, 0.0, 0.999)) << "\n";
}
