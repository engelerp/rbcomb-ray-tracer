#include <camera.hpp>

camera::camera(){
  //double aspect_ratio = 16.0/9.;
  double aspect_ratio = 1.;
  double viewport_height = 1.1;
  double viewport_width = aspect_ratio * viewport_height;
  //double focal_length = 1.0;
  double focal_length = 1001.0;

  origin = point3(0., 0., 1000.);
  horizontal = vec3(viewport_width, 0., 0.);
  vertical = vec3(0., viewport_height, 0.);
  lower_left_corner = origin - horizontal/2. - vertical/2. - vec3(0., 0., focal_length);
  max_bounces = 13;
}

camera::camera(double aspect_ratio, double viewport_height, double viewport_width, double focal_length){
  origin = point3(0., 0., 0.);
  horizontal = vec3(viewport_width, 0., 0.);
  vertical = vec3(0., viewport_height, 0.);
  lower_left_corner = origin - horizontal/2. - vertical/2. - vec3(0., 0., focal_length);
  max_bounces = 20;
}

ray camera::get_ray(double u, double v) const{
  //return ray(origin, lower_left_corner + u*horizontal + v*vertical - origin);
  return ray(lower_left_corner + u*horizontal + v*vertical, unit_vector(lower_left_corner + u*horizontal + v*vertical - origin), 1., 1., 0., 0, max_bounces);
}
