#include <linear_deformer.hpp>
#include <vec3.hpp>
#include <cmath>

linear_deformer::linear_deformer(double maxelong, double z_zero, double up, point3 center)
: maxelong(maxelong), z_zero(z_zero), up(up), center(center){

}

double linear_deformer::z(double x, double y) const{
  double r = std::sqrt((center.x()-x)*(center.x()-x) + (center.y()-y)*(center.y()-y));
  return 2. * up * maxelong * r - up * maxelong + z_zero;
}
