#include <interpolating_deformer.hpp>
#include <vec3.hpp>
#include <cmath>
#include <fstream>
#include <algorithm>

interpolating_deformer::interpolating_deformer(std::string filename, double maxelong, double z_zero, double up, point3 center, double radius)
: maxelong(maxelong), z_zero(z_zero), up(up), center(center), radius(radius){
  //Load interpolation table
  std::fstream file(filename);
  size_t num_points;
  file >> num_points;
  rs = std::vector<double>(num_points);
  zs = std::vector<double>(num_points);
  for(size_t i = 0; i < num_points; ++i){
    file >> rs[i] >> zs[i];
  }
  //Rescale interpolation table
  for(size_t i = 0; i < num_points; ++i){
    rs[i] *= radius;
    zs[i] = z_zero + up * maxelong * zs[i];
  }
}

double interpolating_deformer::z(double x, double y) const{
  double r = std::sqrt((center.x()-x)*(center.x()-x) + (center.y()-y)*(center.y()-y));

  auto it_upper = std::find_if(rs.begin(), rs.end(), [r](double r_int){ return r_int >= r; });
  if(it_upper == rs.begin()){
    return zs.front();
  }
  else if(it_upper == rs.end()){
    return zs.back();
  }
  else{
    double upper_dist = (*it_upper) - r;
    double lower_dist = r - (*(it_upper-1));
    size_t i_upper = it_upper - rs.begin();
    size_t i_lower = i_upper - 1;
    return (lower_dist * zs[i_upper] + upper_dist * zs[i_lower]) / (upper_dist + lower_dist);
  }
}
