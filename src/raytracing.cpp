#include <raytracing.hpp>

double degrees_to_radians(double degrees){
  return degrees * pi / 180.;
}

double random_double(){
  static std::uniform_real_distribution<double> distribution(0., 1.);
  static std::mt19937_64 generator;
  return distribution(generator);
}

double random_double(double min, double max){
  return min + (max - min) * random_double();
}


double clamp(double x, double min, double max){
  if(x < min){
    return min;
  }
  else if(x > max){
    return max;
  }
  else{
    return x;
  }
}
