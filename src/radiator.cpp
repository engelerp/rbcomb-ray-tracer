#include <radiator.hpp>

radiator::radiator(int sourceID): sourceID(sourceID) {

}

bool radiator::scatter(
  const ray& r_in, const hit_record& rec,
  double& fraction_reflected, ray& r_reflected,
  double& fraction_refracted, ray& r_refracted
) const{
  r_reflected = ray(rec.p, r_in.direction(), r_in.get_amplitude(), r_in.optical_path_to(rec.t), r_in.refractive_index_current(), r_in.get_phaseflips(), r_in.get_bounces_left() - 1);
  r_reflected.set_source(sourceID);
  fraction_reflected = 1.;
  return false;
}
