#!/bin/bash -l
#SBATCH --job-name="rbc_rt_0"
#SBATCH --account="eth5b"
#SBATCH --mail-type=ALL
#SBATCH --mail-user=engelerp@phys.ethz.ch
#SBATCH --time=00:05:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-core=2
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=72
#SBATCH --partition=normal
#SBATCH --constraint=mc
#SBATCH --hint=multithread

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

srun ./main 1> image.ppm 2> dbg.txt
